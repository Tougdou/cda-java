package filesHTML;

import java.nio.file.Files;
import java.nio.file.Path;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        
        // Ouverture d'un fichier (le chemin est à changer)
        Path p = Path.of("C:\\Users\\17010-85-01\\Desktop\\filesHTML\\index.html");
        String htmlCode = new String();
        try {
            htmlCode = Files.readString(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        System.out.println("Chemin du fichier à traiter :");
        System.out.println(p.toString());
        System.out.println("Contenu du fichier :");
        System.out.print(htmlCode);
        
        checkTags(htmlCode, "div");
    }
    
    /**
     * Vérifie le fait que les balises du type {@code tag} soient bien ouvertes et fermées.
     * 
     * Exemple :
     * Pour le code suivant :
     * <div>
       *     <h1>Div 1</h1> 
     *     <p>Des choses.</p>
     * </div>
     * <div>
     *     <h1>Div 2</h1> 
     *     <div>Une div</div>
     *     <div>Autre div</div>
     * </div>
     * 
     * La fonction renvoie {@code true}
     *
     * @param htmlCode Le code html à traiter
     * @param tag Le type de balise
     * @return {@code true} si les balises sont correctement ouvertes et fermées, {@code false} sinon
     */
    public static boolean checkTags(String htmlCode, String tag) {
        
        // Faut-il travailler avec une boucle ? Plusieurs ?
        // Passer en revue toute la boucle ?
        // Comment localiser la nature de la balise ?
        
        return false; // Que renvoyer ? Faut-il utiliser une variable ?
    }
}