package fr.afpa.firstapp.firstapp;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainApp extends Application {
	
	private int countCurrentFormation = 10;
	
    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("AFPA countFormation");
        
        VBox root = new VBox(10);
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(30));
        
        Scene scene = new Scene(root, 300, 200);
        
        Label statutLabel = new Label("Nombre de formations AFPA actuel: " + countCurrentFormation);
        
        Button addExerciceButton = new Button ("Une formation en plus !");
        addExerciceButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				countCurrentFormation++;
				statutLabel.setText("Nombre de formations AFPA actuel : " + countCurrentFormation);
			}
		});
               
        Button removeExerciceButton = new Button ("Une formation en moins !");
        
		 removeExerciceButton.setOnAction(new EventHandler<ActionEvent>() {
					
			@Override
			public void handle(ActionEvent event) {
				countCurrentFormation--;
				statutLabel.setText("Nombre de formations AFPA actuel : " + countCurrentFormation);
			}
		});
	     
        root.getChildren().addAll(statutLabel, addExerciceButton, removeExerciceButton);
        
        primaryStage.setScene(scene);
        primaryStage.show();
      
    }
}