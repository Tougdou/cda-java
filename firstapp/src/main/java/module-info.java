module fr.afpa.firstapp {
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires javafx.media;
	requires javafx.graphics;
    opens fr.afpa.firstapp.firstapp to javafx.fxml;
    exports fr.afpa.firstapp.firstapp;
}