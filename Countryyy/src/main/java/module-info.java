module fr.afpa.Countryyy {
    requires transitive javafx.controls;
    requires javafx.fxml;
	requires javafx.base;

    opens fr.afpa.country to javafx.fxml;
    exports fr.afpa.country;
}
