package fr.afpa.country;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;

public class CountryController {

	private ObservableList<Country> listCountriesForComboBox = FXCollections.observableArrayList();
	
	private ObservableList<Country> listCountriesForListView = FXCollections.observableArrayList();
	
	@FXML
	private TableView<Country> tableView;
	
	@FXML
	private Button buttonUpCountry;
	
	@FXML
	private Button buttonDownCountry;
	
	@FXML
	private ComboBox<Country> comboBoxCountry;
	
	@FXML
	private Button buttonAddCountry;
	
	@FXML
	private Button buttonAddAllCountry;
	
	@FXML
	private Button buttonDeleteCountry;
	
	@FXML
	private Button buttonDeleteAllCountry;
	
	@FXML
	private ListView<Country> listViewCountry;
	
	@FXML
	private Button buttonLeave;
	
	//Création de mes objets country à partir de la classe Country
	@FXML
	public void initialize() {
		Country country1 = new Country ("France");
		Country country2 = new Country ("Espagne");
		Country country3 = new Country ("Italie");
		Country country4 = new Country ("Suisse");
		Country country5 = new Country ("Luxembourg");
		Country country6 = new Country ("Belgique");
		Country country7 = new Country ("Ireland");
		Country country8 = new Country ("Pays-Bas");
		Country country9 = new Country ("Allemagne");
		Country country10 = new Country ("Autriche");
		
		//Ajout de tous mes pays à la liste ComboBox
		listCountriesForComboBox.addAll(country1, country2, country3, country4, country5, country6, country7, country8, country9, country10);
		
		//Liasion de ma comboBox à ma liste de country pour la comboBox
		comboBoxCountry.setItems(listCountriesForComboBox);
		
		//Liasion de ma listView à ma liste de country pour la listView
		listViewCountry.setItems(listCountriesForListView);
	}
	
	//Fonction pour ajouter un pays en cliquant sur le bouton en question (event)
	@FXML
    private void addOneCountry(ActionEvent event) {
		//Séléction du pays de ma comboBox stocker dans la variable comboBoxSelectedCountry
		Country comboBoxSelectedCountry = comboBoxCountry.getSelectionModel().getSelectedItem();
		
		if (comboBoxSelectedCountry != null) {
			//Si ma séléction comporte bien un pays alors j'ajoute à ma listView le country séléctionner depuis ma comboBox	
			listCountriesForListView.add(comboBoxSelectedCountry);
			//Si ma séléction comporte bien un pays alors je retire à ma comboBox le country ajouter à ma listView
			listCountriesForComboBox.remove(comboBoxSelectedCountry);
		}
    }
	
	//Fonction pour ajouter tous mes pays en cliquant sur le bouton en question (event)
	@FXML
    private void addAllCountry(ActionEvent event) {
        
		for (int i = 0; i < listCountriesForComboBox.size(); i++) {
			listCountriesForListView.add(listCountriesForComboBox.get(i));
		}
		listCountriesForComboBox.clear();
    }
	
	//Fonction pour retirer un pays en cliquant sur le bouton en question (event)
	@FXML
    private void deleteOneCountry(ActionEvent event) {
		Country listViewSelectedCountry = listViewCountry.getSelectionModel().getSelectedItem();
		
		if (listViewSelectedCountry != null ) {
	        listCountriesForListView.remove(listViewSelectedCountry);
	        listCountriesForComboBox.add(listViewSelectedCountry);			
		}
    }	
	
	//Fonction pour retirer tous mes pays en cliquant sur le bouton en question (event)
	@FXML
    private void deleteAllCountry(ActionEvent event) {
		
		for (int i = 0; i < listCountriesForListView.size(); i++) {
			listCountriesForComboBox.add(listCountriesForListView.get(i));
		}
		
		listCountriesForListView.clear();

    }	
	
	@FXML
	private void upCountry (ActionEvent event) {
		
		int indexCountry = listViewCountry.getSelectionModel().getSelectedIndex();
		
		if (indexCountry > 0 ) {
			Country tempCountry = listCountriesForListView.remove(indexCountry);
			listCountriesForListView.add(indexCountry-1, tempCountry);
		}
	}
	
	@FXML
	private void downCountry (ActionEvent event) {
		
		int indexCountry = listViewCountry.getSelectionModel().getSelectedIndex();
		
		if (indexCountry < listCountriesForListView.size()-1) {
			Country tempCountry = listCountriesForListView.remove(indexCountry);
			listCountriesForListView.add(indexCountry+1, tempCountry);
		}
	}
	
	//Fonction permettant de quitter le programme en cliquant sur le bouton en question (event)
	@FXML
	private void buttonLeave (ActionEvent event) {
		Platform.exit();
	}
}