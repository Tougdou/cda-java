package fr.agglolarochelle.library;

public class Author {

	//ATTRIBUTS
	private String surname;
	private String firstName;
	private String nationality;
	private String editionHouse;
	
	//CONSTRUCTEUR
	public Author(String surname, String firstName, String nationality, String editionHouse) {

		this.surname = surname;
		this.firstName = firstName;
		this.nationality = nationality;
		this.editionHouse = editionHouse;
	}
	
	//GETTERS
	public String getSurname() {
		return surname;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getNationality() {
		return nationality;
	}

	public String getEditionHouse() {
		return editionHouse;
	}
	
	//SETTERS
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public void setEditionHouse(String editionHouse) {
		this.editionHouse = editionHouse;
	}
	
	
}
