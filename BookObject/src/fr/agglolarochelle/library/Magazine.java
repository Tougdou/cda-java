package fr.agglolarochelle.library;

public class Magazine extends Multimedia{
	
	private String editeur;
	private String publicationPeriodique;
	
	public Magazine(String nom, String langue, double price, int sortie, String editeur, String publicationPeriodique) {
		super(nom, langue, price, sortie);

	}

	public String getEditeur() {
		return editeur;
	}

	public String getPublicationPeriodique() {
		return publicationPeriodique;
	}

	public void setEditeur(String editeur) {
		this.editeur = editeur;
	}

	public void setPublicationPeriodique(String publicationPeriodique) {
		this.publicationPeriodique = publicationPeriodique;
	}

}
