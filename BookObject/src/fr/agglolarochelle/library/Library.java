package fr.agglolarochelle.library;

import java.util.ArrayList;
import java.util.HashMap;
					
public class Library {
				
	//ATTRIBUTS
	private ArrayList<Book> allBooks;
	private HashMap<Integer, Multimedia> borrowBooks;
	
	//CONSTRUCTEUR
	public Library(ArrayList<Book> allBooks) {

		this.allBooks = allBooks;
	}
	
	public Library () {
		
		this.allBooks = new ArrayList<Book>();
	}
	
	//METHODS
	public void addBook(Book oneBook) {
		this.allBooks.add(oneBook);
	}
	
	/*public ArrayList<Book> getByAuthor (String authorName) {
	
	
		
		for (int i = 0; i < this.allBooks.size(); i++) {
			
		}
		
		return ;
	}*/
	
	//GETTERS
	public ArrayList<Book> getAllBooks() {
		return this.allBooks;
	}
	
	//SETTERS
	public void setAllBooks(ArrayList<Book> allBooks) {
		this.allBooks = allBooks;
	}
}
