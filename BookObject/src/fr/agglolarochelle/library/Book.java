package fr.agglolarochelle.library;

import java.util.regex.Pattern;

public class Book extends Multimedia{
	
	//ATTRIBUTS
	private String editeur;
	private Author auteur;
	private String isbn;
	private String sujet;
	private String cote;
	private String resume;
	private String statut;
	
	//CONSTRUCTEUR
	public Book(String nom, String langue, double price, int sortie, String editeur, 
			    Author auteur, String isbn, String sujet, String cote, String resume, String statut) {
		super(nom, langue, price, sortie);
		
		this.editeur = editeur;
		this.auteur = auteur;
		this.isbn = isbn;
		this.sujet = sujet;
		this.cote = cote;
		this.resume = resume;
		this.statut = statut;
	}
	
	//METHODS
	
	//Verif ISBN
	private boolean checkIsbn(String isbn) {
		
		if (isbn.length() != 17) {
			return false;
		} 
		
		for (int i = 0; i < isbn.length(); i++) {
			
			char isbnCharCheck = isbn.charAt(i);
			
			if (i != 3 || i != 5 || i != 10 || i != 15) {
				if(Character.isLetter(isbnCharCheck) == false) {
					return false;
				}
			}
			
			if (i == 3 || i == 5 || i == 10 || i == 15) {
				
				if (isbnCharCheck != '-') {
					return false;
				}
			}
		}
		return true;
	}
	
	//Verif côte
	 /**
     * Vérifie le formatage d'une potentielle côte de livre.
     * 
     * @param classificationNumber La chaîne de caractères à traiter.
     * @return True si côte bien formatée.
     */
    private boolean checkClassificationNumber(String classificationNumber) {
    	
        return Pattern.matches("^[0-9]{1,3}[.][0-9]{1,3}[ ][A-Z]{3}$", classificationNumber);
    }
    
    
	//GETTERS recupérer attributs
	public String getEditeur() {
		return editeur;
	}
	
	public Author getAuteur() {
		return auteur;
	}
	
	public String getIsbn() {
		return isbn;
	}
	
	public String getSujet() {
		return sujet;
	}
	
	public String getCote() {
		return cote;
	}
	
	public String getResume() {
		return resume;
	}
	
	public String getStatut() {
		return statut;
	}
	
	//SETTERS changer valeur attributs
	public void setEditeur(String editeur) {
		this.editeur = editeur;
	}
	
	public void setAuteur(Author auteur) {
		this.auteur = auteur;
	}
	
	public void setIsbn(String isbn) {
		
		this.isbn = isbn;
	}
	
	public void setSujet(String sujet) {
		this.sujet = sujet;
	}
	
	public void setCote(String cote) {
		this.cote = cote;
	}
	
	public void setResume(String resume) {
		this.resume = resume;
	}
	
	public void setStatut(String statut) {
		this.statut = statut;
	}
}
