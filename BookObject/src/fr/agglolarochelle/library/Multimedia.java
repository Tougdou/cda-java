package fr.agglolarochelle.library;

import java.time.LocalDate;

abstract public class Multimedia {
	private String nom;
	private String langue;
	private double price;
	private int sortie;
	
	public Multimedia(String nom, String langue, double price, int sortie) {
		super();
		this.nom = nom;
		this.langue = langue;
		this.price = price;
		this.sortie = sortie;
	}
	
	//METHODS
	private boolean checkYear (int yearToCheck) {
    	
    	LocalDate currentDate = LocalDate.now();
    	int currentYear = currentDate.getYear();
    	
    	if (yearToCheck > currentYear) {
    		return false;
    	}
    	
    	return true;
    }

	public String getNom() {
		return nom;
	}

	public String getLangue() {
		return langue;
	}

	public double getPrice() {
		return price;
	}

	public int getSortie() {
		return sortie;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setLangue(String langue) {
		this.langue = langue;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public void setSortie(int sortie) throws IllegalArgumentException {
		
		if (checkYear(sortie) == false) {
			throw new IllegalArgumentException("Cette annee n'existe pas");
		}
		
		this.sortie = sortie;
	}

}
