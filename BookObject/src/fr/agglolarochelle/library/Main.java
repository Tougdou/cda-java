package fr.agglolarochelle.library;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Library myLibrary = new Library();
		
		Author morgan = new Author("Tranquard", "Morgan", "FR", "Clairefontaine");
		Book monPremierLivre = new Book("Music 2010", "Français", 19.99, 2012, "Flammarion", morgan, "978-2-1234-5680-3", "musique", "cote", "blabla", "disponible");
		
		System.out.println(monPremierLivre.getAuteur().getSurname());
		System.out.println(monPremierLivre.getPrice());
		
		Scanner scan = new Scanner(System.in);
		
		boolean errorPresent = true;
		
		do { 
			errorPresent = false;
			System.out.println("Rentre une annee : ");
			int inputYear = scan.nextInt();
			try {
				monPremierLivre.setSortie(inputYear);
			} catch (IllegalArgumentException excp) {
				System.out.println(excp.getMessage());
				errorPresent = true;
			}
			
		} while(errorPresent);
		
		System.out.println("Fin de programme");
		
	}

}
