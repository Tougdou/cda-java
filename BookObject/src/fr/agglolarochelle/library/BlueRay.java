package fr.agglolarochelle.library;

public class BlueRay extends Multimedia{

	private String collector;
	
	public BlueRay(String nom, String langue, double price, int sortie, String collector) {
		super(nom, langue, price, sortie);

	}

	public String getCollector() {
		return collector;
	}

	public void setCollector(String collector) {
		this.collector = collector;
	}

}
