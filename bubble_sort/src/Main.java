import java.util.Random;

public class Main {

	public static void main(String[] args) {
		
		int[] randomNumberArray = new int[30];
		
		generateRandom(100, randomNumberArray);
		System.out.println("Tableau de nombres aleatoires");
		showTable(randomNumberArray);
		System.out.println("\nTableau de nombres aleatoires tries");
		bubbleSort(randomNumberArray);
		showTable(randomNumberArray);
	}
	
	/**
	 * Fonction qui gere l'affichage de mon tableau
	 * @param array Tableau contenant des entiers al�atoires de 0 � 100
	 * @return Pas de retour, la fonction gere que l'affichage de mon tableau
	 */
	public static void showTable(int[] array) {
		
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println("");
	}
	
	public static void generateRandom(int valueMax, int[] arrayToInitialize) {
		
		Random rand = new Random();
		
		for(int i = 0; i < arrayToInitialize.length; i++) {
			int tableNumber = rand.nextInt(valueMax);
			arrayToInitialize[i] = tableNumber;
		}	
	}
	
	public static void bubbleSort (int[] arrayToSort) {
		
		for (int j = 0; j < arrayToSort.length-1; j++) {
			
			for (int i = 0; i < arrayToSort.length-1; i++) {
				
				if(arrayToSort[i] > arrayToSort[i+1]) {
					int safe = arrayToSort[i];
					arrayToSort[i] = arrayToSort[i+1];
					arrayToSort[i+1] = safe;
				}
			}
		}
		
	}	
}
