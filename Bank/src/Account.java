
public class Account {
	
	//ATTRIBUTS
	private int numberAccount;
	private Client owner;
	private double solde;
	private String type;
	

	//CONSTRUCTEUR
	public Account(int numberAccount, Client owner, double solde, String type) {
		this.numberAccount = numberAccount;
		this.owner = owner;
		this.solde = solde;
		this.type = type;
	}
	
	//METHODS
	public void addMoney(double amount) {
		
		this.solde = this.solde + amount;
	}
	
	
	public void removeMoney (double amount ) {
		
		this.solde = this.solde - amount;
	}
	
	public boolean overDrown () {
		
		if (this.solde < 0) {
			return true;
		}
		
		return false;
	}
	
	public void transferMoneyFrom (Account accountSource, double amount) {
		
		if (amount > 0) {
			if (accountSource.solde > amount) {
				this.solde = this.solde + amount;
				accountSource.removeMoney(amount);
			}
		}	
	}
	
	//GETTERS
	public int getNumberAccount() {
		return numberAccount;
	}

	public Client getOwner() {
		return owner;
	}

	public double getSolde() {
		return solde;
	}
	
	public String getType() {
		return type;
	}
	
	//SETTERS
	public void setNumberAccount(int numberAccount) {
		this.numberAccount = numberAccount;
	}

	public void setOwner(Client owner) {
		this.owner = owner;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
	
	
	
	
	
	
	
}
