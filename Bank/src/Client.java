
public class Client {
	
	private String name;
	private String firstName;
	private String job;
	
	public Client(String name, String firstName, String job) {
		this.name = name;
		this.firstName = firstName;
		this.job = job;
	}

	public String getName() {
		return name;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getJob() {
		return job;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setJob(String job) {
		this.job = job;
	}
	
	
}
