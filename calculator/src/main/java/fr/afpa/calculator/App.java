package fr.afpa.calculator;

import java.util.ArrayList;
import java.util.Optional;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
       
    	ArrayList<Integer> numberList = new ArrayList<Integer>();
    	
    	VBox root = new VBox();
    	root.setPadding(new Insets(20)); // Padding top right bottom left
    	
		TextArea inputCalculator = new TextArea("");
		root.getChildren().add(inputCalculator);
		
		GridPane numberGrid = new GridPane();
		root.getChildren().add(numberGrid);
		
		numberGrid.setPadding(new Insets(20));
		numberGrid.setHgap(25);
		numberGrid.setVgap(10);
		numberGrid.setAlignment(Pos.CENTER);
		
		int x = 0; //index x
		int y = 0; //index y
		
		//Boucle l'ensemble de nos boutons correspondant aux chiffres
		for ( int i = 0; i < 10; i++) {
			//Création du bouton qui passera l'index en String
			Button buttonNumber = new Button(Integer.toString(i));
			//Ajout du buttonNumber à notre grille avec les index x et y
			numberGrid.add(buttonNumber, x, y);
			//Création de l'event quand je clique sur un bouton rattaché à un chiffre
			buttonNumber.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					Button buttonSource = (Button)event.getSource();
					//On stock dans strValue la string correspondant au bouton
					String strValue = buttonSource.getText();
					//On stock dans inputCalculatorText la string de notre TextArea
					String inputCalculatorText = inputCalculator.getText();
					//Ajoute dans notre ArrayList la String strValue converti en int
					numberList.add(Integer.parseInt(strValue));
					//Si rien n'est entrée dans notre TextArea on écrit que la valeur de strValue
					if (inputCalculatorText.length() == 0) {
						inputCalculator.setText(strValue);
					//Sinon on écrit ce qui est déja présent en concatenant avec un "+" suivi de la prochaine valeur
					} else {
						inputCalculator.setText(inputCalculatorText + " + " + strValue); 
					}
				}
			});
			
			if (i == 4) {
				x = 0;
			} else  {
				x++;
			}
			
			if (i < 4) {
				y = 0 ;
			} else {
				y = 1;
			}
		}
		
		HBox containerButton = new HBox();
		Button clearButton = new Button("Vider");
		Button calculateButton = new Button("Calculer");
		
		containerButton.setAlignment(Pos.CENTER);

		containerButton.setSpacing(20);
		clearButton.setPadding(new Insets(5));
		calculateButton.setPadding(new Insets(5));
		
		root.getChildren().add(containerButton);
		containerButton.getChildren().add(clearButton);
		containerButton.getChildren().add(calculateButton);
			
		clearButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Alert clearAlert = new Alert(AlertType.CONFIRMATION);
				clearAlert.setTitle("Supprimer le champ");
				clearAlert.setHeaderText("Voulez vous supprimer le champ ?");
				
				Optional<ButtonType> option = clearAlert.showAndWait();
				
				if(option.get() == ButtonType.OK) {
					inputCalculator.clear();
					numberList.clear();
					
				}
			}
		});
		
		calculateButton.setOnAction(new EventHandler<ActionEvent>() {
			
			
			@Override
			public void handle(ActionEvent event) {
				
				int result = 0;
				
				for (int i = 0; i < numberList.size(); i++) {
					result = result + numberList.get(i);
					
				}
				String saveOperation = inputCalculator.getText();
				inputCalculator.setText(saveOperation + " = " + Integer.toString(result));
			}
		});
		
		//Création Container (parent, largeur, hauteur)
  		Scene scene = new Scene(root,500,300);
  		primaryStage.setTitle("Calculator JavaFX 18");
  		primaryStage.setScene(scene);
  		primaryStage.show();
    }

	public static void main(String[] args) {
        launch();
    }

}