module fr.afpa.calculator {
    requires transitive javafx.controls;
	requires java.desktop;
	requires javafx.graphics;
    exports fr.afpa.calculator;
}
