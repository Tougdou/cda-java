import java.io.Serializable;

public class Commercial extends Employee implements Comparable<Commercial>{

	private double turnover;
	private double commission;
	
	public Commercial(String name, String firstName, int age, double brutSalary, double chargesRateSocial, double turnover, double commission) {
		super(name, firstName, age, brutSalary, chargesRateSocial);
		
		this.turnover = turnover;
		this.commission = commission;

	}
	
	@Override
	public String toString() {
		return "Commercial : " + this.getName() + "/" + this.getFirstName() + "/" + this.getAge() + "/" + this.calculateNetSalary();
	}

	public double calculateNetSalary() {
		
		double result = this.getBrutSalary() + this.getTurnover() * this.getCommission();
		
		return result;
	}
	
	public static void displayNetSalaryCalculation() {
		System.out.println("Salaire net + commission");
	}
	
	public int compareTo(Commercial commercial2) {
		
		if (this.calculateNetSalary() < commercial2.calculateNetSalary()) {
			return -1;
		}
		
		if (this.calculateNetSalary() > commercial2.calculateNetSalary()) {
			return 1;
		} 
		
		return 0;
	}
	
	public double getTurnover() {
		return turnover;
	}

	public double getCommission() {
		return commission;
	}

	public void setTurnover(double turnover) {
		this.turnover = turnover;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}
	
	
}
