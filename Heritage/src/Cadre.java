
public class Cadre extends Employee{
	
	public Cadre(String name, String firstName, int age, double brutSalary) {
		super(name, firstName, age, brutSalary, 0.6);
		
		
	}

	@Override
	public String toString() {
		return "Cadre : " + this.getName() + "/" + this.getFirstName() + "/" + this.getAge() + "/" + this.calculateNetSalary();
	}
	
	public double calculateNetSalary() {
		
		double result = this.getBrutSalary() - this.getBrutSalary() * this.getChargesRateSocial();
		
		return result;
	}

	
}
