import java.io.Serializable;

abstract public class Employee implements Serializable{

	private String name;
	private String firstName;
	private int age;
	private double brutSalary;
	private double chargesRateSocial;
	
	public Employee(String name, String firstName, int age, double brutSalary, double chargesRateSocial) {
		this.name = name;
		this.firstName = firstName;
		this.age = age;
		this.brutSalary = brutSalary;
		this.chargesRateSocial = chargesRateSocial;
	}
	
	abstract public double calculateNetSalary();
	
	public String getName() {
		return name;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getAge() {
		return age;
	} 
	
	public double getBrutSalary() {
		return brutSalary;
	}
	
	public double getChargesRateSocial() {
		return chargesRateSocial;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setbrutSalary(double brutSalary) {
		this.brutSalary = brutSalary;
	}

	public void setChargesRateSocial(double chargesRateSocial) {
		this.chargesRateSocial = chargesRateSocial;
	}
	
}
