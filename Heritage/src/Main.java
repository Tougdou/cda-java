import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

public class Main  {

	public static void main(String[] args) {
		
		Commercial commercial1 = new Commercial("Tranquard","Morgan", 31, 2000, 0.2, 1500, 0.1);
		Commercial commercial2 = new Commercial ("Lagrange","Jean Michel", 42, 1700, 0.2, 2500, 0.1);
		Commercial commercial3 = new Commercial ("Dutrou","Jacques", 42, 1800, 0.2, 2200, 0.1);

		ArrayList<Commercial> listCommercials = new ArrayList<Commercial>();
		listCommercials.add(commercial1);
		listCommercials.add(commercial2);
		listCommercials.add(commercial3);
		
		System.out.println("Avant tri : ");
		for (int i = 0; i < listCommercials.size(); i++) {
			System.out.println(listCommercials.get(i));
		}
		
		System.out.println("\nApres tri : ");
		
		Collections.sort(listCommercials);
		for (int i = 0; i < listCommercials.size(); i++) {
			System.out.println(listCommercials.get(i));
		}
		
		serializeCommercials(listCommercials);
		
		ArrayList<Commercial> listCommercialDeserialize = deserializeCommercials("commercials.ser");
		System.out.println(listCommercialDeserialize);
	}
	
	//SERIALIZATION
	public static void serializeCommercials(ArrayList<Commercial> listCommercials) {
		try {
			FileOutputStream fos = new FileOutputStream("commercials.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(listCommercials);
			
			oos.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	//DESERIALIZATION

	@SuppressWarnings("unchecked")
	public static ArrayList<Commercial> deserializeCommercials(String path) {
		
		ArrayList<Commercial> listCommercials = new ArrayList<Commercial>();
		
		try {
			FileInputStream fis = new FileInputStream(path);
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			listCommercials = (ArrayList<Commercial>)ois.readObject();
			
			ois.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return listCommercials;
	}
	
	
}
