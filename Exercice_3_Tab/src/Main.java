
public class Main {

	public static void main(String[] args) {
		
		//Déclaration tableau 2D
		int[][] intArray = new int[2][10];
		
		for(int i = 0; i <= 9; i++) {
			intArray[0][i] = i+1;
			intArray[1][i] = factorial(i+1);
		}
		
		showTable(intArray);
	}
	
	public static int factorial(int n) {
		
		int resultat = 1;
		
		for (int i = 1; i <= n; i++) {
			resultat = resultat * i;
		}
		return resultat;
	}
	
	public static void showTable(int intArray[][]) {
		
		for (int i = 0; i < intArray.length; i++) {
			
			System.out.println();
			
			for (int j = 0; j < intArray[i].length; j++) {
				
				System.out.print(intArray[i][j] + " ");
			}		
		}
	}
}



