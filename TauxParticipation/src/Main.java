import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner userEntry = new Scanner(System.in);
		char restart = 'O';
		while (restart == 'O') {
			
			double result = 0;
			
			double rateAlone = 0.20;
			double rateNotAlone = 0.25;
			System.out.println("Etes-vous seuls ou en couple ? Entrez 1 pour seul ou 0 pour couple");
			int alone = userEntry.nextInt();
			
			if (alone == 1) {
				result = result + rateAlone;
			} else {
				result = result + rateNotAlone;
			}
			
			double rateChild = 0.15;
			System.out.println("Avez-vous des enfants ? Si oui combien ? Si non entrez 0");
			int child = userEntry.nextInt();
			
			if (child > 0) {
				result = result + child * rateChild;
			}
			
			double rateSalary = 0.10;
			System.out.println("Quel est le montant de votre salaire ?");
			int salary = userEntry.nextInt();
			
			if(salary < 1800) {
				result = result + rateSalary;
			}
			
			if(result > 0.5) {
				result = 0.5;
			}
			
			result = Math.ceil(result * 100);
			System.out.println(result + " % de taux de participation employeur");

			System.out.println("Entrez O pour continuer / N pour arreter");
			userEntry.nextLine();
			String userRestart = userEntry.nextLine();
			
			while(!userRestart.equals("O") && !userRestart.equals("N")) {
				System.out.println("Veuillez rentrer le bon caractere pour continuer le programme");
				userRestart = userEntry.nextLine();
			}
			restart = userRestart.charAt(0);
		}
		
		userEntry.close();	
	}
}
