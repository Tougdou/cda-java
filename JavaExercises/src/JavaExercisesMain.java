import java.util.Random;
import java.util.Scanner;

public class JavaExercisesMain {

	public static void main(String[] args) {
		
		
		int[] randomArray = generateRandomNumbersArray(6,50);
		System.out.println("Tableau random de " + randomArray.length + " cases");
		showTable(randomArray);
		
		int sumNumber = sumNumbersAtOddIndex(randomArray);
		System.out.println("Somme des nombres a indexs impairs : " + sumNumber + "\n");
		
		System.out.println("Tableau avant inversion");
		int[] toReverseArray = new int [] {5, 10, 2, 23};
		showTable(toReverseArray);
		
		System.out.println("\nTableau apres inversion");
		int[] reversedArray = reverseArray(toReverseArray);
		showTable(reversedArray);
		
		System.out.println("\nTableau a comparer");
		int[] toCompareArray1 = new int [] {8, 15, 2, 17};
		showTable(toCompareArray1);
		int[] toCompareArray2 = new int [] {8, 15, 2, 17};
		showTable(toCompareArray2);
		boolean isSame = compareArray(toCompareArray1, toCompareArray2);
		if (isSame == true) {
			System.out.println("Vos tableaux sont identiques !");
		} else {
			System.out.println("Vos tableaux ne sont pas identiques !");
		}
		
		String stringTest = new String ("barbecue");
		int[] vowelsConsonant = countVowelsConsonant(stringTest);
		System.out.println("\nNombre de voyelles et de consonnes dans le mot \"" + stringTest + "\"");
		showTable(vowelsConsonant);
		
		String str = new String ("azertyuiop");
		String toLocate = new String ("azerty");
		
		boolean testContains = contains(str,toLocate);		
		System.out.println("\nOn cherche a savoir si la chaine de carac " + toLocate + " est present la chaine de carac " + str);
		if (testContains == true) {
			System.out.println("Bien present !");
		} else {
			System.out.println("Pas present !");
		}
		
		//Scanner userEntry = new Scanner(System.in);
		//System.out.println("\nVeuillez rentrer une ou plusieurs phrases : ");
		String numberString = new String ("Je teste le nombre de phrases. Je crois en avoir plusieurs.");
		System.out.println("\nExemple : " + numberString);
		//String userSentence = userEntry.nextLine();
		int sentencesNumber = countSentences(numberString);	
		System.out.println("Nombres de phrases presentes: " + sentencesNumber);
		//userEntry.close();
		
		String stringExample = new String ("Je teste ma fonction qui consiste a ecrire en camel case");
		String camelCaseResult = sentenceToCamelCase(stringExample);
		System.out.println("\nExemple syntaxe camelCase : " + camelCaseResult);
		
		String stringExample2 = new String ("Adios");
		System.out.println("\nRepetition d'une chaine de carac avec un separateur : ");
		System.out.println(repeatString(stringExample2, 3, '#'));
		
		int[][] array1 = new int [][] {{1,2},{3,4}};
		int[][] array2 = new int [][] {{5,6},{7,8}};
		System.out.print("\nSomme de deux tableaux a 2 dimensions");
		showTable(sum2DArrays(array1, array2));
		
		String rgbString = new String ("rbg(25,10,255)");
		System.out.println("\nConversion d'une string RGB en tableau d'entier");
		showTable(rgbStringToIntArray(rgbString));
		
		String allString = new String ("Help me im bored of this");
		System.out.println("\nCiblage d'une sous chaine dans une chaine principale");
		System.out.println(subString(allString, 3, 8));
		
		String lowerCaseString = new String ("JE teste LE NOMBRE DE MINUSCULES");
		System.out.println("\nJe cherche le nombre de minuscules dans la phrase : " + lowerCaseString );
		System.out.println(countLowerCase(lowerCaseString));
	}
	
	
	public static void showTable(int[] array) {
		
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println("");
	}
	
public static void showTable(int[][] array) {
		
		for (int i = 0; i < array.length; i++) {
			System.out.println();
			for (int j = 0; j < array.length; j++) {
				System.out.print(array[i][j] + " ");
			}
		}
		System.out.println("");
	}
	
	public static int[] generateRandomNumbersArray(int length, int valueMax) {
		Random rand = new Random();
		
		int[] randomNumbersArray = new int[length];
			for (int i = 0; i < randomNumbersArray.length; i++) {
				randomNumbersArray[i] = rand.nextInt(valueMax);
			}
		return randomNumbersArray;
	}
	
	/**
	 * Somme tous les éléments situés aux index impairs.
	 * 
	 * @param array Le tableau à traiter.
	 * @return La somme des éléments.
	 */
	public static int sumNumbersAtOddIndex(int[] array) {
		int result = 0;
		
		for (int i = 0; i < array.length; i++) {
			if(i % 2 != 0) {
				result += array[i];
			}
		}
		
		return result;
	}
	
	/**
	 * Crée un nouveau tableau mirroir de celui passé en paramètre.
	 * 
	 * Exemple :
	 * [5, 10, 2, 23] en entrée doit donner [23, 2, 10, 5]
	 * 
	 * @param toReverse Tableau à traiter
	 * @return Nouveau tableau mirroir
	 */
	public static int[] reverseArray(int[] toReverse) {
		int[] reversed = new int[toReverse.length];
		int j = 0;
		
		for (int i = toReverse.length-1; i >= 0; i--) {
			reversed[j] = toReverse[i];
			j++;
		}
		return reversed;
	}

	/**
	 * Compare les valeurs de deux tableaux d'entiers.
	 * On considère que les tableaux sont de taille identique, la différence de taille n'est pas à gérer.
	 * 
	 * @return {@code true} si les tableaux sont identiques, {@code false} sinon.
	 */
	public static boolean compareArray(int[] array1, int[] array2) {
		
		boolean isSame = false;
		
		for (int i = 0; i < array1.length; i++) {
			if(array1[i] == array2[i]) {
				isSame = true;
			} else {
				return false;
			}
		}
		return isSame;
	}
	
	/**
	 * Compte le nombres de voyelles et de consonnes dans une chaîne de caractères.
	 * 
	 * @param str La chaîne à traiter
	 * @return Un tableau de 2 éléments, l'élément à l'index 0 content le nombre de voyelles et l'élément à l'index 1 contient le nombre de consonnes.
	 */
	public static int[] countVowelsConsonant(String str) {
		
		int[] letterTypeCount = new int[2];
		letterTypeCount[0] = 0;
		letterTypeCount[1] = 0;
		
		for (int i = 0; i <= str.length()-1; i++) {
			if (str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u' || str.charAt(i) == 'y') {
				letterTypeCount[0]++;
			}else {
				letterTypeCount[1]++;
			}
		}
		
		// pour récpérer un caractère dans une chaîne de caractère il est possible d'utiliser str.charAt(index)
		// une comparaison de caractères peut se faire avec l'opérateur ==
		// exemple : str.charAt(index) == 'a'
		
		return letterTypeCount;
	}
	
	/**
	 * Renvoie "true" si la chaîne {@code str} contient la chaîne {@code subStr}.
	 * 
	 * Attention aux taille d'entrée des deux paramètres.
	 * 
	 * @param str Chaîne à traiter
	 * @param subStr Sous-chaîne à retrouver
	 * @return {@code true} si la chaîne {@code subStr} est contenue dans {@code str}. {@code false} sinon.
	 */
	public static boolean contains(String str, String toLocate) {
		
		boolean contains = false;
		int i = 0;
		int count = 0;
		
		while (i <= str.length()-1 && count < toLocate.length()) {
			
			for (int j = 0; j <= toLocate.length()-1; j++) {
				
				if (str.charAt(i) == toLocate.charAt(j)) {
					count++;
					contains = true;
				} else {
					contains = false;
					break;
				}
				i++;
			}
			i++;
		}
		return contains;
	}
	
	/**
     * Compte le nombre de phrase d'une chaîne de caractère passée en paramètre.
     * 
     * Exemple :
     * "Une première phrase. Et une seconde !" renvoie 2
     * 
     * @param toProcess
     * @return
     */
    public static int countSentences(String toProcess) {
        int sentenceCount = 0;
        
        for (int i = 0; i < toProcess.length(); i++) {
        	if (toProcess.charAt(i) == '.' || toProcess.charAt(i) == '?'|| toProcess.charAt(i) == '!') {
        		sentenceCount++;
        	}
        }
           
        return sentenceCount;
    }
    
    /**
     * Transforme une phrase donnée en un nom suivant le standard camel case avec une minuscule au début.
     * On considèrera que la chaîne passée en paramètre est tout en minuscule.
     * 
     * Exemple :
     * Pour la chaîne "nom de variable" la fonction doit retourner "nomDeVariable"
     * 
     * @param toProcess la chaîne de caractère à transformer. 
     * @return La chaîne de caractère en camel case avec première lettre en minuscule (la fonction en instancie une nouvelle)
     */
    public static String sentenceToCamelCase(String toProcess) {
        String camelCaseResult = new String();
        
        for (int i = 0; i < toProcess.length(); i++) {
        	
        	if(toProcess.charAt(i) == ' ' ) {
        		camelCaseResult = camelCaseResult + Character.toUpperCase(toProcess.charAt(i+1));
        		i++;
        	} else {
        		camelCaseResult = camelCaseResult + toProcess.charAt(i);
        	}
        }
        
        return camelCaseResult;
    }
    
    /**
     * Crée une nouvelle chaîne contenant une répétition de la chaîne en paramètre
     * chaque répétition devra être séparée d'un caractère passé en paramètre {@code separator}
     * 
     * Exemple :
     * La chaîne "Bonjour" à répéter avec le séparateur "#" et 3 répétition donne "Bonjour#Bonjour#Bonjour"
     * 
     * @param toRepeat La chaîne de caractère à répéter 
     * @param repeatCount Le nombre de fois qu'il faut répéter la chaîne
     * @param separator Le séparateur de chaîne
     * @return Nouvelle chaîne avec répétition
     */
    public static String repeatString(String toRepeat, int repeatCount, char separator) {
    	
        String result = new String();
        
        while (repeatCount > 0) {
        	if(repeatCount == 1) {
        		result = result + toRepeat;
        	} else {
            	result = result + toRepeat + separator;
        	}
        	repeatCount--;
        }
          
        return result;
    } 
    
    /**
     * Somme des tableaux 2D d'entiers de même taille (autrement appelé "addition matricielle).
     * 
     * Exemple :
     * 5 6      1 2     6 8
     * 4 3   +  4 3  =  8 6
     * 1 2      3 2     4 4
     * 
     * @param array1 Premier tableau à sommer
     * @param array2 Second tableau à sommer
     * @return La somme des tableaux à 2 dimensions
     */
    public static int[][] sum2DArrays(int[][] array1, int[][] array2) {
        int[][] sumArray = new int[array1.length][array1[0].length];
        
        for (int i = 0; i < sumArray.length; i++) {
        	for (int j = 0; j < sumArray.length; j++) {
        		sumArray[i][j] = array1[i][j] + array2[i][j];
        	}
        }
        return sumArray;
    } 
    
    /**
     * Transformer une chaîne de caractère sous la forme rgb(<code-rouge>, <code-vert>, <code-blue>)
     * En un tableau de int correspondant à ces même codes.
     * 
     * Exemple :
     * Paramètre d'entrée : rgb(255,60,19)
     * Tableau de sortie : [255, 60, 19]
     * 
     * @param toProcess Chaîne de caractère correspondant au code RGB à convertir
     * @return
     */
    public static int[] rgbStringToIntArray(String toProcess) {
        // dans ce cas nous sommes obligé d'utiliser la classe ArrayList
        // car nous ne connaissons pas à l'avance le nombre d'entiers qui seront dans la chaîne
        // Un tableau int[] a une taille fixe et il est impossible d'y ajouter des éléments dynamiquement
        int[] rgbIntArray = new int[3];
        String colorCode = new String();
        int colorIndex = 0;
        
        for (int i = 4; i <= toProcess.length()-1; i++) {
        	
        	if(toProcess.charAt(i) == ',' || i == toProcess.length()-1) {
        		int saveCodeColor = Integer.parseInt(colorCode);
        		rgbIntArray[colorIndex] = saveCodeColor;
        		colorCode = "";
        		colorIndex++;
        	} else {
        		colorCode += toProcess.charAt(i);
        	}
        }

        return rgbIntArray;
    }
    
    /**
     * Extrait une sous-chaîne de caractères.
     * 
     * @param start Index de début de la sous-chaîne à extraire
     * @param end Index de fin de la sous-chaîne à extraire
     * @return Une sous chaîne de caractères.
     */
    public static String subString(String str, int start, int end) {
    	
    	String subString = new String();
    	
    	for (int i = start; i <= end; i++) {
    		subString +=  str.charAt(i);
    	}
    	
        return subString;
    }
    
    
    /**
     * Compte le nombre de caractères minuscules d'une chaîne.
     * 
     * @param str La chaîne à traiter.
     * @return Le nombre de caractères en minuscule.
     */
    public static int countLowerCase(String str) {
    	
    	int numberLowerCase = 0;
    	
    	for (int i = 0; i < str.length(); i++) {
    		
    		if (Character.isLowerCase(str.charAt(i))) {
    			numberLowerCase++;
    		}
    	}
    	
        return numberLowerCase;
    }
}