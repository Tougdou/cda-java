import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Random rand = new Random();
		
		int[] randomNumberArray = new int[40];
		int limitNumber = 100;
		
		for(int i = 0; i < randomNumberArray.length; i++) {
			int tableNumber = rand.nextInt(limitNumber);
			randomNumberArray[i] = tableNumber;
		}	
		
		showTable(randomNumberArray);
		
		int min = 0;
		min = findMin(randomNumberArray);
		System.out.println(min +" est le nombre le plus petit du tableau.");
		
		int max = 0;
		max = findMax(randomNumberArray);
		System.out.println(max +" est le nombre le plus grand du tableau.");
		
		float moyenne = 0;
		moyenne = calculateAverage(randomNumberArray);
		System.out.println( moyenne + " est la moyenne de mon tableau de nombre aleatoire.");	
		
		int[] minMax = null;
		minMax = findMinMax(randomNumberArray);
		showTable(minMax);
	}
	
	/**
	 * Fonction qui g�re l'affichage de mon tableau
	 * @param array Tableau contenant des entiers al�atoires de 0 � 100
	 * @return Pas de retour, la fonction g�re que l'affichage de mon tableau
	 */
	public static void showTable(int[] array) {
		
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println("");
	}
	
	/**
	 * Fonction qui cherche le nombre minimum dans mon tableau 
	 * @param array Tableau contenant des entiers
	 * @return L'�l�ment qui correspond au minimum du tableau
	 */
	public static int findMin(int[] array) {
		
		int min = 0; 
		
		for (int i = 0; i < array.length; i++) {
			if (i == 0) {
				min = array[0];
			}
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}
	
	/**
	 * Fonction qui cherche le nombre maximum dans mon tableau 
	 * @param array Tableau contenant des entiers
	 * @return L'�l�ment qui correspond au maximum du tableau
	 */
	public static int findMax(int[] array) {
		
		int max = 0;
		
		for (int i = 0; i < array.length; i++) {
			if (i == 0) {
				max = array[0];
			}
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}
	
	/**
	 * Fonction qui cherche la moyenne des nombre al�atoires du tableau 
	 * @param array Tableau contenant des entiers
	 * @return L'�l�ment qui correspond � la moyenne des valeurs du tableau (type float)
	 */
	public static float calculateAverage(int[] array) {
		
		float result = 0;
		
		for (int i = 0; i < array.length; i++) {
			result = result + array[i];
		}
		float average = result / array.length;
		return average;
	}
	
	/**
	 * Fonction qui cherche le minimum et le maximum des nombres al�atoires du tableau 
	 * @param array Tableau contenant des entiers
	 * @return Le tableau d'entier contenant le minimum et le maximum
	 */
	public static int[] findMinMax(int[] array) {

		int[] arrayMinMax = new int[2];
			
		for (int i = 0; i < array.length; i++) {
			
			if (i == 0) {
				arrayMinMax[0] = array[0];
				arrayMinMax[1] = array[0];			
			}
			
			if (array[i] < arrayMinMax[0]) {
				arrayMinMax[0] = array[i];
			}
			
			if (array[i] > arrayMinMax[1]) {
				arrayMinMax[1] = array[i];
			}		
		}
		return arrayMinMax;
	}
}