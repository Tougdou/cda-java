module fr.afpa.firstMVC {
    requires transitive javafx.controls;
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.base;
	opens fr.afpa.firstMVC to javafx.fxml;
    exports fr.afpa.firstMVC;
}
