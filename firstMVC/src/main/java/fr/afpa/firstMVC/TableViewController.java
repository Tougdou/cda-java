package fr.afpa.firstMVC;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class TableViewController {
	
	@FXML
	private TableView<Personne> tableView;
	
	@FXML
	private TableColumn<Personne, String> firstNameTableColumn;
	
	@FXML
	private TableColumn<Personne, String> surNameTableColumn;
	
	@FXML
	private TableColumn<Personne, String> cityTableColumn;
	
	@FXML
	private Label title;
	
	@FXML
	private Label firstNameLabel;
	
	@FXML
	private Label surNameLabel;
	
	@FXML
	private Label cityLabel;
	
	@FXML
	private TextField firstNameInput;
	
	@FXML
	private TextField surNameInput;
	
	@FXML
	private TextField cityInput;
	
	@FXML
	private Button saveButton;
	
	@FXML
	private Button cancelButton;
	
	@FXML
	private Button deleteButton;
	
	private ObservableList<Personne> personsModel = FXCollections.observableArrayList();
	
	//METHODS
	
	public void saveForm(MouseEvent event) {
		personsModel.add(new Personne (this.firstNameInput.getText(), this.surNameInput.getText(), this.cityInput.getText()));
	}
	
	public void cancelForm(MouseEvent event) {
		firstNameInput.clear();
		surNameInput.clear();
		cityInput.clear();
	}
	
	public void deleteUser(MouseEvent event) {
		Personne deleteUser = tableView.getSelectionModel().getSelectedItem();
		personsModel.remove(deleteUser);
	}
	
	@FXML
	public void initialize() {
		personsModel.add(new Personne ("Alain", "Chevalier", "Strasbourg"));
		personsModel.add(new Personne ("Ali", "Messaoudi", "Bayonne"));
		personsModel.add(new Personne ("Alexandre", "Berbille", "La Rochelle"));
		
		tableView.setItems(personsModel);
		
		firstNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().getFirstName());
		surNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().getSurName());
		cityTableColumn.setCellValueFactory(cellData -> cellData.getValue().getCity());
	}
}
