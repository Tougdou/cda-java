package fr.afpa.firstMVC;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Personne {
	
	private StringProperty firstName;
	private StringProperty surName;
	private StringProperty city;
	
	public Personne(String firstName, String surName, String city) {
		super();
		this.firstName = new SimpleStringProperty(firstName);
		this.surName = new SimpleStringProperty(surName);
		this.city = new SimpleStringProperty(city);
	}

	public StringProperty getFirstName() {
		return firstName;
	}

	public StringProperty getSurName() {
		return surName;
	}

	public StringProperty getCity() {
		return city;
	}

	public void setFirstName(StringProperty firstName) {
		this.firstName = firstName;
	}

	public void setSurName(StringProperty surName) {
		this.surName = surName;
	}

	public void setCity(StringProperty city) {
		this.city = city;
	}
}
