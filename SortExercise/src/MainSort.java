import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainSort {

	public static void main(String[] args) {
		
		int[] table = new int[] {8,24,5,17,14};
		
		ArrayList<Integer> dynamicTable = new ArrayList<>(List.of(7,15,26,9,18));
		
		System.out.println("Tableau soft avant tri :");
		showTable(table); // Affichage avant tri
		
		System.out.println("Tableau dynamique avant tri :");
		showTable(dynamicTable); // Affichage avant tri
		
		System.out.println("Tableau soft apres tri a bulle:");
		bubbleSort(table);
		showTable(table);
		
		System.out.println("Tableau dynamique apres tri a bulle:");
		bubbleSort(dynamicTable); // Triage du tableau avec la méthode tri à bulle
		showTable(dynamicTable); // Affichage après tri à bulle
		
		System.out.println("Tableau soft apres tri a selection:");
		selectionSort(table);
		showTable(table);
		
		System.out.println("Tableau dynamique apres tri a selection:");
		selectionSort(dynamicTable);
		showTable(dynamicTable);
		
		int[] table2 = new int[] {1,10,1,6,4,5};
		countingSort(table2, 11);
		System.out.println("Tableau test apres tri par comptage : ");
		showTable(table2);
		
		Scanner userEntryInt = new Scanner(System.in);
		Scanner userEntryString = new Scanner(System.in);
		
		int[] generalTable = new int[10];
		System.out.println("\nChoisissez la valeur max des nombres aleatoires : ");
		int valueMax = userEntryInt.nextInt();
		generateRandom(valueMax, generalTable);
		System.out.println("\nChoisissez une methode de tri S pour selection / B pour bulle / C pour comptage : ");
		String typeOfSort = userEntryString.nextLine();
		
		if (typeOfSort.equals("S") || typeOfSort.equals("s")){
			selectionSort(generalTable);
			showTable(generalTable);
		}
		if (typeOfSort.equals("B") || typeOfSort.equals("b")) {
			bubbleSort(generalTable);
			showTable(generalTable);
		}
		if (typeOfSort.equals("C") || typeOfSort.equals("c")) {
			countingSort(generalTable,11);
			showTable(generalTable);
		}
		userEntryInt.close();	
		userEntryString.close();	
		
	}
	
	public static void showTable(int[] table) {
		for(int i = 0; i < table.length; i++) {
			System.out.print(table[i] + " ");
		}
		System.out.println("\n");
	}
	
	public static void showTable(ArrayList<Integer> dynamicTable) {
		for(int i = 0; i < dynamicTable.size(); i++) {
			System.out.print(dynamicTable.get(i) + " ");
		}
		System.out.println("\n");
	}
	
	public static void bubbleSort(int[] arrayToSort) {
		
		for (int j = 0; j < arrayToSort.length-1; j++) {
			
			for (int i = 0; i < arrayToSort.length-1; i++) {
				
				if(arrayToSort[i] > arrayToSort[i+1]) {
					int safe = arrayToSort[i];
					arrayToSort[i] = arrayToSort[i+1];
					arrayToSort[i+1] = safe;
				}
			}
		}
	}
	
	public static void generateRandom(int valueMax, int[] arrayToInitialize) {
		
		Random rand = new Random();
		
		for(int i = 0; i < arrayToInitialize.length; i++) {
			int tableNumber = rand.nextInt(valueMax);
			arrayToInitialize[i] = tableNumber;
		}	
	}
	
	public static void bubbleSort(ArrayList<Integer> arrayToSort) {
		
		for (int j = 0; j < arrayToSort.size()-1; j++) {
			
			for (int i = 0; i < arrayToSort.size()-1; i++) {
				
				if(arrayToSort.get(i) > arrayToSort.get(i+1)) {
					
					int safe = arrayToSort.get(i);
					
					arrayToSort.set(i, arrayToSort.get(i+1));		
					arrayToSort.set(i+1, safe);
				}
			}
		}
	}
	
	public static void selectionSort(int[] arrayToSort) {
		
		for (int i = 0; i <= arrayToSort.length-2; i++) {
			int target = i;
			int min = 0;
			
			for (int minIndex = i; minIndex <= arrayToSort.length-1; minIndex++) {
				
				min = arrayToSort[target];
				if(min > arrayToSort[minIndex]) {
					min = arrayToSort[minIndex];
					target = minIndex;
				}
			}
			
			int temp = arrayToSort[i];
			arrayToSort[i] = min;
			arrayToSort[target] = temp;
		}
	}
	
	public static void selectionSort (ArrayList<Integer> arrayToSort) {
		
		for (int i = 0; i < arrayToSort.size()-1; i++) {
			int target = i;
			int min = 0;
			
			for (int minIndex = i; minIndex < arrayToSort.size()-1; minIndex++) {
				min = arrayToSort.get(target);
				if(min > arrayToSort.get(minIndex)) {
					min = arrayToSort.get(minIndex);
					target = minIndex;
				}
			}
			
			int temp = arrayToSort.get(i);
			arrayToSort.set(i, min);
			arrayToSort.set(target, temp);
		}
	}
	
	public static void countingSort (int[] arrayToSort, int valueMax) {
		
		int[] collectArray = new int[valueMax];
		
		for (int i = 0; i < arrayToSort.length; i++) {
			
			collectArray[arrayToSort[i]]++;
		}
		
		int indexArrayToSort = 0;
		
		for (int i = 0; i < collectArray.length; i++) {
			
			if (collectArray[i] > 0) {
				
				for (int setCount = 0; setCount < collectArray[i]; setCount++) {
					
					arrayToSort[indexArrayToSort] = i;
					indexArrayToSort++;
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

