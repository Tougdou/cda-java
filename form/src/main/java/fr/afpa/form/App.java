package fr.afpa.form;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class App extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		GridPane root = new GridPane();

		root.setPadding(new Insets(20)); // Padding top right bottom left
		root.setHgap(25); // Ecart horizontal
		root.setVgap(15); // Ecart vertical

		//Etiquette premier champ
		Label labelFirstInput = new Label("Entrée utilisateur :");
		//Premier champ
		TextField firstInput = new TextField("Saisissez un texte");
		//Bouton copier 
		Button copyButton = new Button("Recopier");
		
		//Bouton effacer
		Button clearButton = new Button("Effacer");
		
		//Etiquette second champ 
		Label labelSecondInput = new Label("Copie de l'entrée :");
		//Second champ
		TextField secondInput = new TextField("Saisissez un texte");
		secondInput.setDisable(true);
		//Bouton quitter
		Button leaveButton = new Button("Quitter");
		
		//EVENT COPIER
		copyButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				secondInput.setText(firstInput.getText());
			}
		});
		
		//EVENT EFFACER
		clearButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				firstInput.clear();
				secondInput.clear();
			}
		});
		
		//EVENT QUITTER
		leaveButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Platform.exit();
			}
		});
		
		//Positionnement éléments
		//Grid (x,y) (vertical,horizontal)
		root.add(labelFirstInput, 0, 1);
		root.add(firstInput, 1, 1);
		root.add(copyButton, 2, 1);

		root.add(clearButton, 2, 2);

		root.add(labelSecondInput, 0, 3);
		root.add(secondInput, 1, 3);
		root.add(leaveButton, 2, 3);
		
		//Création Container (parent, hauteur, largeur)
		Scene scene  = new Scene(root,500,200);
		primaryStage.setTitle("Form JavaFX 18");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch();
	}

}