import java.util.ArrayList;

public class Film {
	
	//ATTRIBUTS
	public String filmName;
	public String producer;
	public String category;
	public double durationFilm;
	public int filmRealease;
	public ArrayList<Double> gradeNumberList = new ArrayList<Double>(); 
	
	//CONSTRUCTEUR
	public Film(String filmName, String producer, String category, double durationFilm, int filmRealease) {
		this.filmName = filmName;
		this.producer = producer;
		this.category = category;
		this.durationFilm = durationFilm;
		this.filmRealease = filmRealease;
	}
	
	//METHODS
	public void addGrade (double grade) {
		
		this.gradeNumberList.add(grade);

	}
	
	public double averageGrade () {
		
		double result = 0;
		
		for (int i = 0; i < this.gradeNumberList.size(); i++) {
			
			result = result + this.gradeNumberList.get(i);
		}
		
		result = result / this.gradeNumberList.size();
		
		return result;
	}
	
	public String getFilmName() {
		return filmName;
	}

	public String getProducer() {
		return producer;
	}

	public String getCategory() {
		return category;
	}

	public double getDurationFilm() {
		return durationFilm;
	}

	public int getFilmRealease() {
		return filmRealease;
	}

	public ArrayList<Double> getGradeNumberList() {
		return gradeNumberList;
	}

	public void setFilmName(String filmName) {
		this.filmName = filmName;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setDurationFilm(double durationFilm) {
		this.durationFilm = durationFilm;
	}

	public void setFilmRealease(int filmRealease) {
		this.filmRealease = filmRealease;
	}

	public void setGradeNumber(ArrayList<Double> gradeNumberList) {
		this.gradeNumberList = gradeNumberList;
	}
	
	
	
}
