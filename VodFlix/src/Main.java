
public class Main {

	public static void main(String[] args) {
		
		Film film1 = new Film("Avatar", "Cameron","SF", 2.42, 2009);
		Film film2 = new Film("Avatar2", "Cameron","SF", 2.42, 2015);
		Film film3 = new Film("Avatar3", "Cameron","SF", 2.42, 2018);
		Film film4 = new Film("Avatar4", "Cameron","SF", 2.42, 2020);
		Film film5 = new Film("Avatar5", "Cameron","SF", 2.42, 2022);
		
		Client client1 = new Client("Lagrange", "Jean-Michel", 43, "lagrange.jm@gmail.com", "07.77.00.07.85");
		
		client1.addFilmToRent(film1);
		client1.addFilmToRent(film2);
		client1.addFilmToRent(film3);
		client1.addFilmToRent(film4);
		client1.addFilmToRent(film5);
		
		client1.displayRentedFilms();
	}

}
