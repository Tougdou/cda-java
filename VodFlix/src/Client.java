import java.util.ArrayList;

//CLASS Client
public class Client {
	
	//ATTRIBUTS
	private String name;
	private String firstName;
	private int age;
	private String email;
	private String phoneNumber;
	private ArrayList<Film> rentedFilmList = new ArrayList<Film>();
	
	//CONSTRUCTEUR
	public Client(String name, String firstName, int age, String email, String phoneNumber) {
		this.name = name;
		this.firstName = firstName;
		this.age = age;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	
	//METHODS
	public void addFilmToRent(Film rentedFilm) {
		this.rentedFilmList.add(rentedFilm);
	}
	
	public void deleteFirstRentedFilm() {
		
		if (this.rentedFilmList.size() > 0) {
			
			this.rentedFilmList.remove(0);
		}		
	}
	
	public void deleteLastRentedFilm() {

		if (this.rentedFilmList.size() > 0) {
			
			this.rentedFilmList.remove(rentedFilmList.size()-1);
		}	
	}
	
	public void displayRentedFilms() {
		
		for (int i = 0; i < this.rentedFilmList.size(); i++) {
			System.out.println(this.rentedFilmList.get(i).getFilmName());
		}
	}
	
	public int getNumberRentedFilms( ) {
		
		return this.rentedFilmList.size();
	}
	
	//GETTERS
	public String getName() {
		return name;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getAge() {
		return age;
	}

	public String getEmail() {
		return email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public ArrayList<Film> getRentedFilm() {
		return rentedFilmList;
	}
	
	//SETTERS
	public void setName(String name) {
		this.name = name;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setRentedFilm(ArrayList<Film> rentedFilm) {
		this.rentedFilmList = rentedFilm;
	}
	
}
