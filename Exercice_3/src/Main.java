import java.util.Scanner;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Random rand = new Random();
		Scanner scanObj = new Scanner(System.in);
		
		int limitNumber = 20;
	
		int numberRandom = rand.nextInt(limitNumber) + 1;
		System.out.println("Un nombre aléatoire a été tiré !");
		System.out.print("Veuillez entrer une proposition comprise entre 1 et 20: ");
		
		int userEntry = 0;
		
		do {
			userEntry = scanObj.nextInt();	
		} while (userEntry > 20);
		
		int limitChoice = 4;
		
		while (numberRandom != userEntry && limitChoice > 0) {
			if (numberRandom > userEntry) {
				System.out.println("Le nombre tiré est plus grand, il vous reste "+ limitChoice + " tentatives");
			} else {
				System.out.println("Le nombre tiré est plus petit, il vous reste " + limitChoice + " tentatives");
			}	
			
			limitChoice = limitChoice -1;
			System.out.println("Veuillez entrer une nouvelle proposition : ");
			userEntry = scanObj.nextInt();
			
			while (userEntry > 20) {
				System.out.println("Veuillez entrer une nouvelle proposition comprise entre 1 et 20 : ");
				userEntry = scanObj.nextInt();
			}
		}
		
		if (numberRandom == userEntry) {
			System.out.print("Bravo vous avez retrouvé le nombre aléatoire attendu :" + numberRandom );
		} else {
			System.out.print("Dommage vous n'avez pas retrouvé le nombre aléatoire attendu");
		}

		scanObj.close();
	}
}