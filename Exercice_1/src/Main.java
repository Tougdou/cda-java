import java.util.Scanner;

public class Main {
 	
	public static void main(String[] args) {		
		
		//Nom de classe - nom d'objet = (new) operateur d'instanciation de l'objet appelant un constructeur de la classe
		Scanner scanObj = new Scanner(System.in);
		System.out.print("Ecrire un nombre: ");
		int userEntry = scanObj.nextInt();
		
		int sum = sumNumber(userEntry);
				
		System.out.println("La somme des N premiers nombres entiers est :" + sum);
		scanObj.close();
	}	
	
	public static int sumNumber(int nMax) {
		int sum = 0;
		
		for(int i = 1; i <= nMax; i++) {
			sum = sum + i;
		}
		
		return sum;
	}	
//Fin class Main	
}

