import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner userEntry = new Scanner(System.in);
		
		System.out.print("Entrer un premier nombre compris entre 1 et 9 : ");
		int firstEntry = userEntry.nextInt();
		
		System.out.println("Entrer un deuxième nombre compris entre 1 et 9 : ");
		int secondEntry =userEntry.nextInt();
			
		int[][] intArray = generateTable();
		showTable(intArray);
		
		System.out.print("\nLe resulatat de la multiplication est : " + intArray[firstEntry-1][secondEntry-1]);
		
	}
	
	public static int[][] generateTable() {

		int[][] intArray = new int[9][9];
		
		for (int i = 0; i < intArray.length; i++) {
			
			for (int j = 0; j < intArray[i].length; j++) {
				
				intArray[i][j] = (i+1) * (j+1);
			}		
		}
		return intArray;
	}
	
	public static void showTable(int intArray[][]) {
		
		for (int i = 0; i < intArray.length; i++) {
			
			System.out.println();
			
			for (int j = 0; j < intArray[i].length; j++) {
				
				System.out.print(intArray[i][j] + " ");
			}		
		}
	}
}
