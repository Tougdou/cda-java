package firstERP;
import java.time.LocalDate;
import java.util.ArrayList;

import firstERP.Employee;

public class main {

	public static void main(String[] args) {
		
		Employee employee1 = new Employee ("15ABC10","Michmuch","Nicolas",1200, 0.5, LocalDate.of(1998, 10, 31));
		Employee employee2 = new Employee ("15ABC11","Azerty","Tom",1200, 0.5, LocalDate.of(1998, 10, 31));
		Employee employee3 = new Employee ("15ABC12","Polere","Jacques",1200, 0.5, LocalDate.of(1998, 10, 31));
		
		ArrayList<Employee> listEmployee = new ArrayList<Employee>();
		listEmployee.add(employee1);
		listEmployee.add(employee2);
		listEmployee.add(employee3);
		
		for (int i = 0; i < listEmployee.size(); i++) {
			Employee emp = listEmployee.get(i);

			System.out.println(emp.getMatricule() + " " + emp.getFirstName() + " " + emp.getSurName() + " " + emp.getBrutSalary() + " " + emp.netSalary() + " " + emp.getBirthday());
		}
	}

}

