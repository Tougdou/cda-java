package firstERP;
import java.time.LocalDate;

public class Employee {
	private String matricule;
	private String surName;
	private String firstName;
	private double brutSalary;
	private double socialChargesRate;
	private LocalDate birthday;
	
	public Employee (String matricule, String surname, String firstName, double brutSalary, double socialChargesRate, LocalDate birthday) {
		this.matricule = matricule;
		this.surName = surname;
		this.firstName = firstName;
		this.brutSalary = brutSalary;
		this.socialChargesRate = socialChargesRate;
		this.birthday = birthday;
	}
	
	public boolean checkMatricule(String matricule){
		
		if (matricule.length() != 7) {
			return false;
		} 
		
		for (int i = 0; i < matricule.length(); i++) {
			
			char charCheck = matricule.charAt(i);

			if ( i == 0 || i == 1 || i == 5 || i == 6) {
				if(Character.isDigit(charCheck) == false) {
					return false;
				}
			}

			if ( i == 2 || i == 3 || i == 4) {
				if(Character.isLetter(charCheck) == false) {
					return false;
				}
			}
		}
		return true; 
	}
	
	public boolean checkName (String name) {
		
		for (int i = 0; i < name.length(); i++) {
			
			char charCheck = matricule.charAt(i);
			
			if (Character.isLetter(charCheck) == false) {
				return false;
			}
		}
		return true;
	}
	
	public boolean checkSocialChargesRate (double socialChargesRate) {
		
		if (socialChargesRate > 0.60  ) {
			return false;
		}
		
		return true;
	}
	
	public double netSalary() {
		
		double netSalary = this.brutSalary - this.brutSalary * this.socialChargesRate;
			
		return netSalary;
	}
	
	public String getMatricule() {
		return matricule;
	}
	
	public void setMatricule(String matricule) throws IllegalArgumentException {
		
		if (!checkMatricule(matricule)) {
			throw new IllegalArgumentException("Matricule invalide");
		}
		
		this.matricule = matricule;
	}
	
	public String getSurName() {
		return surName;
	}
	
	public void setSurName(String surName) throws IllegalArgumentException{
		
		if (checkName(surName) == false) {
			throw new IllegalArgumentException("Votre nom de famille doit comporter que des lettres");
		}
			
		this.surName = surName;
		
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		
		if (checkName(firstName) == false) {
			throw new IllegalArgumentException("Votre prénom doit comporter que des lettres");
		}
		
		this.firstName = firstName;
	}
	
	public double getBrutSalary() {
		return brutSalary;
	}
	
	public void setBrutSalary(double brutSalary) {
		this.brutSalary = brutSalary;
	}
	
	public double getSocialChargesRate() {
		return socialChargesRate;
	}
	
	public void setSocialChargesRate(double socialChargesRate) throws IllegalArgumentException {
		
		if (checkSocialChargesRate(socialChargesRate) == false) {
			throw new IllegalArgumentException("Taux de charges sociales trop haut");
		}
		this.socialChargesRate = socialChargesRate;
	}
	
	public LocalDate getBirthday() {
		return birthday;
	}
	
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
}


