import java.util.Random;
public class Main {

	public static void main(String[] args) {
		
		int tableLength = 5;
		// 1. Générer un tableau 2D de nombres aléatoires d'entiers compris entre 0 et 100
		// Comment appeler generateRandomTable ?
		// Attention, nouveauté, la taille est passée en paramètre (tableLength est déclaré ligne 6 et est à utiliser).
		
		int[][] randomTable = generateRandomTable(tableLength);
		showTable(randomTable);
		
		//J'appelle directement ma fonction qui me retourne result dans mon instruction d'affichage
		System.out.print("\nLa somme des nombres aleatoires de notre tableau est : " + sumAllNumbers(randomTable));
		
		System.out.print("\nLa somme des nombres aleatoires en diagonales de notre tableau est : " + sumDiagonalNumbers(randomTable));
		
		System.out.print("\nLa somme des nombres aleatoires des bordures de notre tableau est : " + sumBordersNumbers(randomTable));
		
	}

	public static void showTable(int[][] table) {
		for(int i = 0; i < table.length; i++) {
			System.out.print("\n");
			for(int j = 0; j < table[i].length; j++) {
				System.out.print(table[i][j] + " ");
			}
		}
	}
	
	public static int[][] generateRandomTable(int tableLength) {
		int[][] randomTable = new int[tableLength][tableLength];
		
		// Code à compléter
		// Pour rappel : 
		// - utilisation de 2 boucles imbriquées pour passer en revue toutes les cases du tableau
		// - la génération d'un nombre aléatoire entre 0 et 100 peut se faire en utilisant le code suivant :
		// Random rand = new Random();
		// int randomNumber = rand.nextInt(100);
		
		Random rand = new Random();
		
		for (int i = 0; i < randomTable.length; i++) {
			
			for (int j = 0; j < randomTable[i].length; j++) {
				randomTable[i][j] = rand.nextInt(100);
			}		
		}
		
		return randomTable;
	}
	
	/**
	 * Somme tous les nombres contenus dans un tableay 2D d'entiers.
	 * 
	 * @return La somme (un entier) des nombres du tableau
	 */
	public static int sumAllNumbers(int[][] table) {
		int result = 0;
		
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				
				result = result + table[i][j];
			}
		}	
		return result;
	}
	
	/**
	 * Somme des nombres situés sur la diagonale qui part du haut-gauche et qui va vers le bas-droit du tableau.
	 * 
	 * Exemple :
	 * 
	 * 2  1  3
	 * 5  6  8
	 * 6  5  3
	 * 
	 * Donne le résultat : 2 + 6 + 3
	 * 
	 * @return La somme (un entier) des nombres de la diagonale du tableau
	 */
	public static int sumDiagonalNumbers(int[][] table) {
		
		int result = 0;
		int i = 0;
		int j = 0;
		
		while (i < table.length && j < table.length) {

			result = result + table[i][j];
			
			i++;
			j++;
		}
		
		return result;
	}
	
	/**
	 * Somme les nombres des bords du tableau.
	 * 
	 * Exemple :
	 * 
	 * 2  1  3
	 * 5  6  8
	 * 6  5  3
	 * 
	 * Donne le résultat : 
	 * 2 + 1 + 3 (première ligne) 
	 * + 5 + 6 (première colonne sans compter le 2 qui a déjà été pris en compte) 
	 * + 8 + 3 (dernière colonne sans compter le 3 qui a déjà été pris en compte)
	 * + 5 (sans compter 6 et 3 qui ont déjà été pris en compte)
	 */ 
	public static int sumBordersNumbers(int[][] table) {
		
		int result = 0;
		
		for (int j = 0; j < table.length; j++) {
			result += table [0][j];
		}
		
		for (int i = 1; i < table.length; i++) {
			result += table [i][table.length-1];
		}
		
		for (int j = 0; j < table.length-1; j++) {
			result += table [table.length-1][j];
		}
		
		for (int i = 1; i < table.length-1; i++) {
			result += table [i][0];
		}
		
		return result;
	}
}
