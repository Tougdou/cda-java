package fr.afpa.learning;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.Vector;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ArrayTest {

	@Test
	void testCompareArray() {
		int[] array1 = new int[] {9, 8, 7, 6, 5, 4, 32};
		int[] array2 = new int[] {9, 8, 7, 6, 5, 4, 32};
		int[] array3 = new int[] {9, 8, 7};
		
		Assertions.assertTrue(ArrayExercises.compareArray(array1, array2));
		Assertions.assertFalse(ArrayExercises.compareArray(array1, array3));
	}

	@Test
	void testReverseArray() {
		int[] array1 = new int[] {100, 8, 7, 6, 5, 4, 32};
		int[] arrayMirrored = new int[] {32, 4, 5, 6, 7, 8, 100};
		
		Assertions.assertArrayEquals(ArrayExercises.reverseArray(array1), arrayMirrored);
	}

	@Test
	void testSumNumbersAtOddIndexIntArray() {
		int[] array1 = new int[] {100, 8, 7, 6, 5, 4, 32, 28};
		
		Assertions.assertEquals(ArrayExercises.sumNumbersAtOddIndex(array1), 46);
	}

	@Test
	void testSumNumbersAtOddIndexVectorOfInteger() {
		Vector<Integer> vec1 = new Vector<Integer>();
		Collections.addAll(vec1, 100, 8, 7, 6, 5, 4, 32, 28);
		
		Assertions.assertEquals(ArrayExercises.sumNumbersAtOddIndex(vec1), 46);
	}

	@Test
	void testAddArrays() {
		fail("Not yet implemented");
	}

	@Test
	void testSumDiagonalNumbers() {
		fail("Not yet implemented");
	}

	@Test
	void testSumBordersNumbers() {
		fail("Not yet implemented");
	}

	@Test
	void testSumAllNumbers() {
		fail("Not yet implemented");
	}

	@Test
	void testDichotomicSearch() {
		fail("Not yet implemented");
	}

	@Test
	void testWordSearch() {
		fail("Not yet implemented");
	}

}
