package fr.afpa.learning;

import java.util.Collection;
import java.util.Collections;
import java.util.Vector;

public class StringExercises {
	
	/**
	 * Compte le nombre de caractères de la chaîne.
	 * 
	 * @param str La chaîne à traiter
	 * @param c Le caractère à compter
	 * @return Le nombre de caractères comptés
	 */
	
	public static int countChar(String str, char c) {
		
		// une comparaison de caractères peut se faire avec la méthode Character.compare(car1, car2)
		// par exemple Character.compare(str.charAt(0), c)
		
		int countChar = 0;
		
		for (int i = 0 ; i < str.length(); i++) {
			if (c == str.charAt(i)) {
				 countChar++;
			}
		}
		return countChar;
	}
	
	/**
	 * Transforme la chaîne de caractères passée en paramètre de façon à la mettre en minuscule.
	 * 
	 * @param str La chaîne de caractères à transformer
	 * @return La chaîne de caractères transformée
	 */
	public static String toLowerCase(String str) {
		
		String lowerCaseString = str.toLowerCase();
		
		return lowerCaseString;
	}
	
	/**
	 * Transforme la chaîne de caractères passée en paramètre de façon à la mettre en majuscule.
	 * 
	 * @param str La chaîne de caractères à transformer
	 * @return La chaîne de caractères transformée
	 */
	public static String toUpperCase(String str) {
		
		String upperCaseString = str.toUpperCase();
		
		return upperCaseString;
	}
	
	/**
	 * Renvoie le mirroir d'une chaîne de caractères.
	 * Un nouvel objet chaîne est à instancier. 
	 *
	 * Exemple : "javajava" donne "avajavaj"
	 * 
	 * Pour accéder à un char
	 * 
	 * @param str Chaîne à traiter
	 * @return La chaîne en mirroir
	 */
	public static String mirrorString(String str) {
		String mirrored = new String();
		StringBuilder strb = new StringBuilder(str);
		
		mirrored = strb.reverse().toString();
		
		return mirrored;
	}
	
	/**
	 * Insérer une chaîne de caractères dans une autre.
	 * 
	 * Vous pouvez, si vous le souhaitez, utiliser un objet de la classe StringBuffer 
	 * pour manipuler une String dynamique.
	 * 
	 * @param str Chaîne à traiter
	 * @param toInsert Chaîne à insérer
	 * @param index Position d'insertion, si l'index >= à la taille de la chaîne à traiter la fonction ajoute la chaîne {@code toInsert} à la fin.
	 * si l'index est négatif, la chaîne renvoyée est la même que celle passée en paramètre.
	 * @return Une nouvelle chaîne de caractères 
	 */
	public static String insertString(String str, String toInsert, int index) {
		
		StringBuffer sb = new StringBuffer(str);
		
		if (index >= 0) {
			if (index < str.length()+1) {
				sb.insert(index, toInsert);
			} else {
				index = str.length();
				sb.insert(index, toInsert);
			}
		}
			
		return sb.toString();
	}
 	
	/**
	 * Vérifie si une chaîne de caractères passée en paramètre est en camelCase (avec première lettre en minuscule).
	 * 
	 * @param name La chaîne à vérifier.
	 * @return {@code true} si la chaîne est bien en camelCase.
	 */
	public static boolean isCamelCaseCompliant(String str) {
		
		for (int i = 0; i < str.length(); i++) {
			
			if (i == 0) {
				boolean isUpper = Character.isUpperCase(str.charAt(i));
				if (isUpper == true) {
					return false;
				}
			}
			
			if (str.charAt(i) == ' ') {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Transforme une chaîne de caractères et retourne sa version en camelCase.
	 * Tout caractère non alphanumérique devra être supprimé.
	 * 
	 * Par exemple : la fonction transforme la chaîne "nombre maximum" en "nombreMaximum".
	 * 
	 * @param str La chaîne de caractères à transformer
	 * @return La chaîne de caractères transformée
	 */ 
	public static String camelCaseConverter(String str) {
		
		String camelCaseResult = new String();
	        
        for (int i = 0; i < str.length(); i++) {
        	
        	if (i == 0) {
        		char recupChar = Character.toLowerCase(str.charAt(0));
        		camelCaseResult = camelCaseResult + recupChar;
        	} else if (str.charAt(i) == ' ' || !Character.isLetterOrDigit(str.charAt(i))) {
        		if (i != str.length()-1) {
        			camelCaseResult = camelCaseResult + Character.toUpperCase(str.charAt(i+1));
            		i++;
        		}
			} else {
				camelCaseResult = camelCaseResult + str.charAt(i);
        	}
        }
        
        return camelCaseResult;
	}
	
	/**
	 * Extrait une sous-chaîne de caractères.
	 * 
	 * @param start Index de début de la sous-chaîne à extraire
	 * @param end Index de fin de la sous-chaîne à extraire (le caractère indexé par ce paramètre doit être compris dans l'extraction)
	 * @return Une sous chaîne de caractères.
	 */
	public static String subString(String str, int start, int end) {
		
		String subString = new String();
		
		if (end > str.length()) {
			end = str.length()-1;
		}
		
		if (start >= 0) {
			
			for (int i = start; i <= end; i++) {
				subString += str.charAt(i);
			}			
		}
		
		return subString;
	}
	
	/**
	 * Compte le nombre de caractères minuscules d'une chaîne.
	 * 
	 * @param str La chaîne à traiter.
	 * @return Le nombre de caractères en minuscule.
	 */
	public static int countLowerCase(String str) {
		
		int numberLowerCase = 0;
		
		for (int i = 0; i < str.length(); i++) {
			if (Character.isLowerCase(str.charAt(i))) {
				numberLowerCase++;
			}
		}
		
		return numberLowerCase;
	}
	
	/**
	 * Renvoie "true" si la chaîne {@code str} contient la chaîne {@code subStr}.
	 * La casse n'est pas à prendre en compte.
	 * 
	 * Attention aux taille d'entrée des deux paramètres.
	 * 
	 * @param str Chaîne à traiter
	 * @param subStr Sous-chaîne à retrouver
	 * @return {@code true} si la chaîne {@code subStr} est contenue dans {@code str}. {@code false} sinon.
	 */
	public static boolean contains(String str, String toLocate) {
		
		if (str.contains(toLocate)) {
			return true;
		} 
		
		String lowerCaseString = toLocate.toLowerCase();
		
		if (str.contains(lowerCaseString)) {
			return true;
		}
			
		return false;
	}
	
	/**
	 * Vérifie si la chaîne de caractères passée en paramètre est une phrase composée de plusieurs mots qui 
	 * débute par une majuscule et est conclue par un point de tout type.
	 * 
	 * Exemple :
	 *  - La chaîne "Ceci est pas une phrase." est une phrase.
	 *  - La chaîne "mot" n'est pas une phrase.
	 *  - La chaîne "Phrase non ponctuée" n'est pas considérée comme une phrase.
	 * 
	 * @param str Chaîne à traiter.
	 * @return true si la chaîne de caractères est une phrase.
	 */
	public static boolean checkIfCorrectSentence(String str) {
		
		char charAtEnd = str.charAt(str.length()-1);
		
		if (charAtEnd != '.' && charAtEnd != '?' && charAtEnd != '!') {
			return false;
		}
		if (Character.isLowerCase(str.charAt(0)) == true) {
			return false;
		}
		
		if (!str.contains(" ")) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Renvoie un tableau de chaînes de caractères contenant les mots d'une phrase.
	 * La ponctuaction est à considérer comme ne faisant pas partie d'un mot.
	 * 
	 * Par exemple, pour la chaîne "J'aime les framboises." la fonction devra retourner le tableau :
	 * ["J'aime", "les", "framboises"]
	 * 
	 * Méthode pouvant être utilisée : split() de la classe String.
	 * Tutoriel pour l'utilisation de split : https://codegym.cc/fr/groups/posts/la-methode-string-split-en-java
	 * 
	 * @param str La chaîne à traiter
	 * @return Les mots extraits de la phrase
	 */
	public static Vector<String> convertSentenceIntoArray(String str) {
		
		 String[] sentenceArray = str.split(" ");
				
		 Vector<String> sentenceVectorArray = new Vector<String>();
	
          Collections.addAll(sentenceVectorArray, sentenceArray);
	
		return sentenceVectorArray;
	}
	
	/**
	 * Retrouve toutes les occurences potentielles d'une sous-chaîne de caractères dans celle passée en paramètre.
	 * 
	 * @param str 
	 * @return Le tableau des index de début des sous-chaînes retrouvées.
	 */
	public static Vector<Integer> findSubStrings(String str, String toLocate) {
		
		 
		 for (int i = 0; i < str.length(); i++) {
			 if (str.charAt(i) == toLocate.charAt(i)) {
				  
				 
			 }
		 }
		 
		int countSubStrings = 0;
		
		Vector<Integer> subStringsIndexes = new Vector<Integer>();
		
		subStringsIndexes.add(firstIndexSubstring);
		return new Vector<Integer>();
	}
	
	/**
	 * Crée une nouvelle chaîne contenant une répétition de la chaîne en paramètre
	 * chaque répétition devra être séparée d'un caractère passé en paramètre {@code separator}
	 * 
	 * Exemple :
	 * La chaîne "Bonjour" à répéter avec le séparateur "#" et 3 répétition donne "Bonjour#Bonjour#Bonjour"
	 * 
	 * @param toRepeat La chaîne de caractères à répéter 
	 * @param nbRepeat Le nombre de fois qu'il faut répéter la chaîne
	 * @param separator Le séparateur de chaîne
	 * @return Nouvelle chaîne avec répétition
	 */
	public static String repeatString(String toRepeat, int repeatCount, char separator) {
		String result = new String();
		return result;
	}
	
	/**
	 * Transformer une chaîne de caractères sous la forme rgb(<code-rouge>, <code-vert>, <code-blue>)
	 * en un tableau de int correspondant à ces mêmes codes.
	 * 
	 * Exemple :
	 * Paramètre d'entrée : rgb(255,60,19)
	 * Tableau de sortie : [255, 60, 19]
	 * 
	 * @param toProcess Chaîne de caractères correspondant au code RGB à convertir
	 * @return
	 */
	public static int[] rgbStringToIntArray(String toProcess) {
		// dans ce cas nous sommes obligé d'utiliser la classe ArrayList
		// car nous ne connaissons pas à l'avance le nombre d'entiers qui seront dans la chaîne
		// Un tableau int[] a une taille fixe et il est impossible d'y ajouter des éléments dynamiquement
		int[] rgbIntArray = new int[3];
		String colorCode = new String();
		int colorIndex = 0;
		
		for (int i = 4; i <= toProcess.length(); i++) {
			if(toProcess.charAt(i) == ',' || i == toProcess.length()-1) {
        		int saveCodeColor = Integer.parseInt(colorCode);
        		rgbIntArray[colorIndex] = saveCodeColor;
        		colorCode = "";
        		colorIndex++;
        	} else {
        		colorCode += toProcess.charAt(i);
        	}
		}

		// Vérification si un caractère est un chiffre :
		// boolean isDigit(char ch)
		// Exemple d'appel de cette fonction :
		// Character.isDigit(toProcess.get(index));

		
		
		return rgbIntArray;
	}
	
	/**
	 * Transforme un numéro de téléphone passé sous la forme "07 52 46 23 52" (pas la peine de l'essayer, c'est aléatoire)
	 * sous la forme avec un préfixe du pays (+33752462352)
	 * 
	 * @param phoneNumber Numéro de téléphone sous la forme traditionnelle
	 * @param prefix Le préfixe à ajouter au début (en anglais "prepend")
	 * @return Numéro de téléphone correctement formaté
	 */
	public static String formatPhoneNumber(String phoneNumber, String prefix) {
		return new String();
	}
}