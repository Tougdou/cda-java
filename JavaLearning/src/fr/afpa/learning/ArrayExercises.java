package fr.afpa.learning;

import java.util.Random;
import java.util.Vector;

public class ArrayExercises {
	
	/**
	 * Compare les valeurs de deux tableaux d'entiers.
	 * Attention à bien gérer la différence de taille des tableaux.
	 * 
	 * @return {@code true} si les tableaux sont identiques, {@code false} sinon.
	 */
	public static boolean compareArray(int[] array1, int[] array2) {
		// Une version avec des boucles est attendue.
		return true;
	}
	
	/**
	 * Crée un nouveau tableau mirroir de celui passé en paramètre.
	 * 
	 * Exemple :
	 * [5, 10, 2, 23] en entrée doit donner [23, 2, 10, 5]
	 * 
	 * @param toReverse Tableau à traiter
	 * @return Nouveau tableau mirroir
	 */
	public static int[] reverseArray(int[] toReverse) {
		int[] reversed = new int[toReverse.length];
		return reversed;
	}
	
	/**
	 * Somme tous les éléments situés aux index impairs.
	 * 
	 * @param array Le tableau à traiter.
	 * @return La somme des éléments.
	 */
	public static int sumNumbersAtOddIndex(int[] array) {
		int result = 0;
		
		// Il est possible de savoir si un entier est pair ou impair en utilisant l'opérateur modulo donnant le reste d'une division entière : %
		// un nombre pair à un reste de division par 2 nul.
		
		return result;
	}
	
	/**
	 * Somme tous les éléments situés aux index impairs.
	 * La particularité de cette méthode réside dans le fait qu'elle prend en paramètre un Objet de la classe Vector.
	 * Vector est une implémentation de tableau dynamique (il est possible d'ajouter et de supprimer des éléments).
	 * 
	 * Documentation de la classe Vector : https://docs.oracle.com/javase/8/docs/api/java/util/Vector.html
	 * 
	 * @param array Le tableau à traiter.
	 * @return La somme des éléments.
	 */
	public static int sumNumbersAtOddIndex(Vector<Integer> array) {
		int result = 0;
		// Questions à se poser :
		// Comment accéder à un élément du tableau représenté par Vector ?
		
		return result;
	}
	
	/**
	 * Somme des tableaux 2D d'entiers de même taille (autrement appelée addition matricielle).
	 * 
	 * Exemple :
	 * 5 6      1 2     6 8
	 * 4 3   +  4 3  =  8 6
	 * 1 2      3 2     4 4
	 * 
	 * @param array1 Premier tableau à sommer
	 * @param array2 Second tableau à sommer
	 * @return La somme des tableaux à 2 dimensions
	 */
	public static int[][] addArrays(int[][] array1, int[][] array2) {
		int[][] sumArray = new int[array1.length][array1[0].length];
		
		return sumArray;
	}
	
	public static int sumOddLines(int[][] array) {
		int result = 0;
		return result;
	}
	
	/**
	 * Somme des nombres situés sur la diagonale qui part du haut-gauche et qui va vers le bas-droit du tableau.
	 * 
	 * Exemple :
	 * 
	 * 2  1  3
	 * 5  6  8
	 * 6  5  3
	 * 
	 * Donne le résultat : 2 + 6 + 3
	 * 
	 * @param toProcess Tableau à traiter.
	 * @return La somme (un entier) des nombres de la diagonale du tableau
	 */
	public static int sumDiagonalNumbers(int[][] toProcess) {
	
		return -1;
	}
	
	/**
	 * Somme les nombres des bords du tableau.
	 * 
	 * Exemple :
	 * 
	 * 2  1  3
	 * 5  6  8
	 * 6  5  3
	 * 
	 * Donne le résultat : 
	 * 2 + 1 + 3 (première ligne) 
	 * + 5 + 6 (première colonne sans compter le 2 qui a déjà été pris en compte) 
	 * + 8 + 3 (dernière colonne sans compter le 3 qui a déjà été pris en compte)
	 * + 5 (sans compter 6 et 3 qui ont déjà été pris en compte)
	 */ 
	public static int sumBordersNumbers(int[][] toProcess) {
		
		return -1;
	}
	
	/**
	 * Somme tous les nombres contenus dans un tableau 3D d'entiers.
	 * 
	 * @return La somme (un entier) des nombres du tableau
	 */
	public static int sumAllNumbers(int[][][] toProcess) {
		
		return -1;
	}
	
	/**
	 * Cherche en utilisant un approche dichotomique la valeur du paramètre {@code searchedValue}.
	 * 
	 * @param searchedValue La valeur recherchée
	 * @return
	 */
	public static int dichotomicSearch(int[] toProcess, int searchedValue) {
		
		// Une approche récusive est recommandée.
		return -1;
	}
	
	/**
	 * Cherche un mot dans un tableau 2D de caractères et renvoie les positions de sa première lettre
	 * et sa denrière lettre.
	 * Le mot à localiser peut être écrit verticalement, horizontalement ou même en diagonale
	 * 
	 * Exemple :
	 * 
	 *  e f b l p
	 *  a m J c i
	 *  i v A k p
     *  b w V s q 
	 *  l n A m o
	 * 
	 * Dans cet exemple le mot "JAVA" est écrit en vertical.
	 * Sa première lettre est aux index (i, j) = (1, 2)
	 * Sa dernière lettre est aux index (i, j) = (4, 2)
	 * 
	 * @param array Le tableau 2D à traiter.
	 * @return Un tableau 2D d'entiers avec : 1er sous tableau la paire (i, j) de la première lettre du mot et 2ème sous tableau la paire (i, j) de la dernière lettre du mot.
	 */
	public static int[][] wordSearch(char[][] array, String toLocate) {
		
		Random rand = new Random();
		rand.nextInt(64);
		
		return new int[2][2];
	}
}
