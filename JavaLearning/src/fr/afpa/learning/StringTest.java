package fr.afpa.learning;

import java.util.Vector;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StringTest {

	@Test
	void testToLowerCase() {
		String result;
		result = StringExercises.toLowerCase("TEST");
		Assertions.assertTrue(result.equals("test"));
		
		result = StringExercises.toLowerCase("TesT");
		Assertions.assertTrue(result.equals("test"));
		
		result = StringExercises.toLowerCase("Test DE phraSe !");
		Assertions.assertTrue(result.equals("test de phrase !"));
		
		result = StringExercises.toLowerCase("ACCENTS : éèà !");
		Assertions.assertTrue(result.equals("accents : éèà !"));
	}

	@Test
	void testToUpperCase() {
		String result;
		result = StringExercises.toUpperCase("test");
		Assertions.assertTrue(result.equals("TEST"));
		
		result = StringExercises.toUpperCase("TesT");
		Assertions.assertTrue(result.equals("TEST"));
		
		result = StringExercises.toUpperCase("Test DE phraSe !");
		Assertions.assertTrue(result.equals("TEST DE PHRASE !"));

	}

	@Test
	void testMirrorString() {
		String result;
		result = StringExercises.mirrorString("javajava");
		Assertions.assertTrue(result.equals("avajavaj"));

		result = StringExercises.mirrorString("Mirroir, mon beau mirroir !");
		Assertions.assertTrue(result.equals("! riorrim uaeb nom ,riorriM"));
	}

	@Test
	void testInsertString() {
		String result = StringExercises.insertString("Bonjourmonde", " tout le ", 7);
		Assertions.assertTrue(result.equals("Bonjour tout le monde"));

		result = StringExercises.insertString("Java", " bien ou pas?", 100);
		Assertions.assertTrue(result.equals("Java bien ou pas?"));
		
		result = StringExercises.insertString("Java", " bien ou pas?", -100);
		Assertions.assertTrue(result.equals("Java"));
	}

	@Test
	void testIsCamelCaseCompliant() {
		Assertions.assertFalse(StringExercises.isCamelCaseCompliant("ViveLesChameaux"));
		Assertions.assertTrue(StringExercises.isCamelCaseCompliant("viveLesChameaux"));
	}

	@Test
	void testCamelCaseConverter() {
		String result = StringExercises.camelCaseConverter("la framboise");
		Assertions.assertTrue(result.equals("laFramboise"));
		
		result = StringExercises.camelCaseConverter("Nom en Camel case");
		Assertions.assertTrue(result.equals("nomEnCamelCase"));
		
		result = StringExercises.camelCaseConverter("virgule,point.");
		Assertions.assertTrue(result.equals("virgulePoint"));
	}

	@Test
	void testSubString() {
		String result = StringExercises.subString("Extraction de chaîne.", 0, 9);
		Assertions.assertTrue(result.equals("Extraction"));
		
		result = StringExercises.subString("Extraction de chaîne.", 11, 12);
		Assertions.assertTrue(result.equals("de"));
		
		result = StringExercises.subString("Extraction de chaîne.", 14, 100);
		Assertions.assertTrue(result.equals("chaîne."));
	}

	@Test
	void testCountChar() {
		int result = StringExercises.countChar("Caractères en pagaille", 'a');
		Assertions.assertTrue(result == 4);
	}

	@Test
	void testCountLowerCase() {
		int result = StringExercises.countLowerCase("LoWeR CaSe?");
		Assertions.assertTrue(result == 4);
	}

	@Test
	void testContains() {
		boolean result = StringExercises.contains("Needle in a hay stack", "hay");
		Assertions.assertTrue(result);
		
		result = StringExercises.contains("Needle in a hay stack", "HAY");
		Assertions.assertTrue(result);
		
		result = StringExercises.contains("Needle in a hay stack", "HEY");
		Assertions.assertFalse(result);
	}

	@Test
	void testCheckIfCorrectSentence() {
		boolean result = StringExercises.checkIfCorrectSentence("Ceci est une phrase.");
		Assertions.assertTrue(result);
		
		result = StringExercises.checkIfCorrectSentence("Ceci est une phrase enjouée !");
		Assertions.assertTrue(result);
		
		result = StringExercises.checkIfCorrectSentence("Ceci est une phrase avec un caractère bizarre à la fin #");
		Assertions.assertFalse(result);
		
		result = StringExercises.checkIfCorrectSentence("pas de majuscule.");
		Assertions.assertFalse(result);
		
		result = StringExercises.checkIfCorrectSentence("Pas de point, mais une virgule");
		Assertions.assertFalse(result);
	}

	@Test
	void testConvertSentenceIntoArray() {
		String[] expected = new String[] {"Une", "phrase", "avec", "plusieurs", "mots."};
		
		Vector<String> result = StringExercises.convertSentenceIntoArray("Une phrase avec plusieurs mots.");
		Assertions.assertArrayEquals(result.toArray(), expected);
	}

	@Test
	void testFindSubStrings() {
		Vector<Integer> result = StringExercises.findSubStrings("Répétitions ! Ceci est une phrase avec des répétitions, répétitions, répétitions...", "répétitions");
		Assertions.assertArrayEquals(result.toArray(), new Integer[] {0, 43, 56, 69});
	};
}
