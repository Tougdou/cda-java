# Java Learning

Ce projet regroupe des séries d'exercices permettant de travailler sur les bases du langage Java.

## Fonctionnement
Le programme est basé sur JUnit, votre objectif est de modifier les fichiers sources ".java" comprenant le suffixe "Exercies".
Les tests JUnit vous aiguilleront sur le bon fonctionnement de vos algorithmes et vous permettront d'être autonome sur le développement.

## Comment travailler sur le projet ?
Il vous suffit de "cloner" le dépôt et d'ouvrir le projet avec votre IDE de prédilection.

**Bon courage !**
