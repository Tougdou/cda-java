import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner userEntry = new Scanner(System.in);
		
		System.out.println("\nChoisissez une operation arithmetique : ");
		String operateur = userEntry.nextLine();
		
		while (!operateur.equals("+") && !operateur.equals("-") && !operateur.equals("*") && !operateur.equals("/")) {
			System.out.println("Veuillez rentrer un operateur correct : ");
			operateur = userEntry.nextLine();
		}
		
		boolean userEntryIsOk = false;
		float firstNumber = 0;
		
		while (!userEntryIsOk) {
			try {
				System.out.println("Entrez votre premier chiffre / nombre : ");
				firstNumber = userEntry.nextFloat();
				System.out.println("Votre premiere entree est : " + firstNumber);
				userEntryIsOk = true;
			} catch (InputMismatchException errorType) {
				System.out.println("Des chiffres ou des nombres !");
				userEntryIsOk = false;
				userEntry.next();
			}
		}
		
		float secondNumber = 0;
		userEntryIsOk = false;
		
		while (!userEntryIsOk) {
			try {
				System.out.println("Entrez votre deuxieme chiffre / nombre : ");
				secondNumber = userEntry.nextFloat();
				System.out.println("Votre deuxieme entree est : " + secondNumber);
				userEntryIsOk = true;
			} catch (InputMismatchException errorType) {
				System.out.println("Des chiffres ou des nombres !");
				userEntryIsOk = false;
				userEntry.next();
			}
		}
		
		float resultat = 0;
		try {
			resultat = calculate(operateur, firstNumber, secondNumber);
		} catch (ArithmeticException errorDivide) {
			System.out.println("La division par 0 n'existe pas !");
		}
		
		System.out.println("\nLe resultat de l'operation est : " + resultat);
		
		userEntry.close();
	}
	
	public static float calculate (String operateur, float value1, float value2) throws ArithmeticException {
		float resultat = 0;
		
		if (operateur.equals("+")) {
			resultat = value1 + value2;
		}
		else if (operateur.equals("-")) {
			resultat = value1 - value2;
		}
		else if (operateur.equals("*")) {
			resultat = value1 * value2;
		}
		else if (operateur.equals("/")) {
			if (value2 == 0) {
				throw new ArithmeticException(); 
			}
				resultat = value1 / value2;				
		}
		return resultat;
	}
}
