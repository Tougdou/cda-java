module fr.afpa.Photoshop {
    requires transitive javafx.controls;
    requires javafx.fxml;
	requires javafx.base;
	requires javafx.graphics;

    opens fr.afpa.Photoshop to javafx.fxml;
    exports fr.afpa.Photoshop;
}
