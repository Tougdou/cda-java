package fr.afpa.Photoshop;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;


public class PhotoshopController {
	
    @FXML
    private TextField inputTextDefault;
    
    @FXML
    private TextField inputTextTransform;
    
    @FXML
    private CheckBox checkBoxBackgroundColor;
    
    @FXML
    private CheckBox checkBoxFontColor;
    
    @FXML
    private CheckBox checkBoxLetterType;
    
    @FXML
    public RadioButton radioButtonGreenBackground;
    
    @FXML
    private RadioButton radioButtonBlueBackground;
    
    @FXML
    private RadioButton radioButtonRedBackground;
    
    @FXML
    private Slider sliderGreenFont;
    
    @FXML
    private Slider sliderBlueFont;
    
    @FXML
    private Slider sliderRedFont;
    
    @FXML
    private RadioButton radioButtonUppercase;
    
    @FXML
    private RadioButton radioButtonLowerCase;
    
    @FXML
    private ToggleGroup backgroundGroup;
    
    @FXML
    private ToggleGroup caseGroup;
    
    @FXML
    //Event recopie de mon premier input dans le deuxième
    private void eventEntryUserInput(KeyEvent userEntryEvent) {
    	String saveText = inputTextDefault.getText();
    	inputTextTransform.setText(saveText);
    }
    
    @FXML 
    private void eventBackgroundGreen (ActionEvent userBackGreen) {
    	BackgroundFill greenColor = new BackgroundFill(Color.GREEN, null, null); //?
    	inputTextTransform.setBackground(new Background(greenColor));
    }
    
    @FXML 
    private void eventBackgroundBlue (ActionEvent userBackBlue) {
    	BackgroundFill blueColor = new BackgroundFill(Color.BLUE, null, null); //?
    	inputTextTransform.setBackground(new Background(blueColor));
    }
    
    @FXML 
    private void eventBackgroundRed (ActionEvent userBackRed) {
    	BackgroundFill redColor = new BackgroundFill(Color.RED, null, null); //?
    	inputTextTransform.setBackground(new Background(redColor));
    }
    
    @FXML
    private void eventTextColor (MouseEvent userTextColor) {
    	String text = inputTextTransform.getText();
    	Label label = new Label(text);
    	label.setTextFill(new Color(sliderRedFont.getValue(), sliderGreenFont.getValue(), sliderBlueFont.getValue(),1));
    	inputTextTransform.setText(label.toString());
    }
    
    
    
    @FXML
    private void eventTextToUppercase (ActionEvent userTextUppercase) { 
    	String text = inputTextTransform.getText();
    	inputTextTransform.setText(text.toUpperCase());
    }
    
    @FXML
    private void eventTextToLowercase (ActionEvent userTextLowercase) { 
    	String text = inputTextTransform.getText();
    	inputTextTransform.setText(text.toLowerCase());
    }
        //.setStyle("-fx-text-inner-color: #" + red + green + blue); 

    
    
    
    
    
    
    
    }

