import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner scanObj = new Scanner(System.in);
		
		System.out.print("Ecrire le premier nombre: ");
		int userEntry1 = scanObj.nextInt();
		System.out.print("Ecrire le deuxi�me nombre: ");
		int userEntry2 = scanObj.nextInt();
		System.out.print("Ecrire le troisi�me nombre: ");
		int userEntry3 = scanObj.nextInt();
		
		int sumNumber = sum(userEntry1, userEntry2, userEntry3);
		
		System.out.println("Voici votre r�sultat : " + sumNumber);
		scanObj.close();
	}
	
	public static int sum(int valA, int valB, int valC) {
		
		return valA + valB + valC;
	}
}
