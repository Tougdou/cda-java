import java.util.ArrayList;
import java.util.LinkedList;

public class Linky {

	public static void main(String[] args) {
		
		//Création de mon ArrayList attendant en entrée des String
		ArrayList<String> arry = new ArrayList<>();
		
		//Ajout d'élément à ma ArrayList avec la méthode .add
		arry.add("Juliette");
		arry.add("Morgan");
		arry.add("Alexis");
		arry.add("Ludovic");
		
		System.out.println("Ma ArrayList : " + arry);
		System.out.println("\nPremier element de ma liste : " + arry.get(0));
		System.out.println("Dernier element de ma liste : " + arry.get(arry.size()-1));
		System.out.println("Suppression de mon premier element de ma liste : " + arry.remove(0));
		System.out.println("Suppression de mon dernier element de ma liste : " + arry.remove(arry.size()-1));
		System.out.println("\nMa ArrayList apres suppression : " + arry);
		
		System.out.println("\n/////////////////////////////////////////////////////");
		
		//Création de ma LinkedList attendant en entrée des String
		LinkedList<String> linky = new LinkedList<>();
		
		//Ajout d'élément à ma LinkedList avec la méthode .add
		linky.add("Juliette");
		linky.add("Morgan");
		linky.add("Alexis");
		linky.add("Ludovic");
		System.out.println("\nMa LinkedList : " + linky);
		
		System.out.println("\nPremier element de ma liste : " + linky.getFirst());
		System.out.println("Dernier element de ma liste : " + linky.getLast());
		System.out.println("Suppresion du premier element de la liste : " + linky.removeFirst());
		System.out.println("Suppresion du dernier element de la liste : " + linky.removeLast());
		System.out.println("\nMa LinkedList apres suppression: " + linky);
		
		linky.addFirst("Ludovic");
		linky.addLast("Juliette");
		System.out.println("\nMa LinkedList apres mes ajouts en position first et last : " + linky);
	}
}
